#include "validator.hpp"

#include <algorithm>
#include <cstddef>
#include <iostream>
#include <iterator>

namespace td_fluorine
{
  bool Validator::isValidIdentifier(std::string_view identifier) noexcept
  {
    static constexpr auto isDigit{
      [](char ch) {
        return ((ch >= '0') && (ch <= '9'));
      }
    };

    static constexpr auto isCharacter{
      [](char ch) {
        return (((ch >= 'a') && (ch <= 'z')) || ((ch >= 'A') && (ch <= 'Z')));
      }
    };

    if (identifier.empty())
    {
      return false;
    }

    if ((identifier[0] != '_') && !isCharacter(identifier[0]))
    {
      return false;
    }

    return std::all_of(std::next(begin(identifier)), end(identifier), [](char ch) {
      return (isCharacter(ch) || (ch == '_') || isDigit(ch));
    });
  }

  bool Validator::checkOverlap(const ClientStructHolder& clientStructs) noexcept
  {
    bool isOverlapFree{ true };

    for (const auto& clientStruct : clientStructs)
    {
      if (clientStruct.variables().empty())
      {
        continue;
      }

      std::reference_wrapper<const ClientVariable> previousVariable{ *clientStruct.variables().cbegin() };

      std::size_t nextOffset{ 0 };

      for (const auto& clientVariable : clientStruct.variables())
      {
        if (nextOffset > clientVariable.offset())
        {
          isOverlapFree = false;
          std::cerr << "warn: variable "
                    << clientStruct.name() << "::" << clientVariable.name()
                    << " overlaps "
                    << clientStruct.name() << "::" << previousVariable.get().name() << std::endl;
        }

        std::size_t variableSize{ clientVariable.size(clientStructs) };

        nextOffset = (clientVariable.offset() + variableSize);
        previousVariable = clientVariable;
      }
    }

    return isOverlapFree;
  }

  bool Validator::checkIdentifiers(const ClientStructHolder& clientStructs) noexcept
  {
    bool allIdentifiersValid{ true };

    for (const auto& clientStruct : clientStructs)
    {
      if (!isValidIdentifier(clientStruct.name()))
      {
        std::cerr << "warn: struct " << clientStruct.name() << " has an invalid name" << std::endl;
      }

      for (const auto& clientVariable : clientStruct.variables())
      {
        if (!isValidIdentifier(clientVariable.name()))
        {
          std::cerr << "warn: variable " << clientStruct.name() << "::" << clientVariable.name() << " has an invalid name" << std::endl;
        }
      }
    }

    return allIdentifiersValid;
  }

  bool Validator::checkDependencies(const ClientStructHolder& clientStructs) noexcept
  {
    bool result{ true };

    for (const auto& clientStruct : clientStructs)
    {
      if (!clientStruct.parentName().empty() && !clientStructs.contains(clientStruct.parentName()))
      {
        std::cerr << "warn: " << clientStruct.name() << " is a child of " << clientStruct.parentName() << ", but " << clientStruct.parentName() << " doesn't exist" << std::endl;
        result = false;
      }

      for (const auto& clientVariable : clientStruct.variables())
      {
        if (clientVariable.type() == source_sdk::fieldtype_t::FIELD_EMBEDDED)
        {
          if (!clientStructs.contains(clientVariable.embeddedType()))
          {
            std::cerr << "warn: " << clientStruct.name() << "::" << clientVariable.name() << " has type " << clientVariable.embeddedType() << ", but " << clientVariable.embeddedType() << " doesn't exist" << std::endl;
            result = false;
          }
        }
      }
    }

    return result;
  }

  bool Validator::validateClientStructs(const ClientStructHolder& clientStructs) noexcept
  {
    bool allValid{ true };

    // Overlapping variables are allowed.
    /* allValid &= */ checkOverlap(clientStructs);
    allValid &= checkIdentifiers(clientStructs);
    allValid &= checkDependencies(clientStructs);

    return allValid;
  }
}