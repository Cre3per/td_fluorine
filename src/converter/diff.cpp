#include "diff.hpp"
#include <iostream> // TODO: remove

#include "data_source/file_source.hpp"
#include "intermediate_representation.hpp"

#include <algorithm>
#include <cassert>
#include <filesystem>
#include <stdexcept>
#include <utility>

namespace td_fluorine::converter
{
  bool Diff::addToIgnoreList(std::string_view ignore)
  {
    for (int i{ 0 }; i < static_cast<int>(Change::What::count); ++i)
    {
      auto what{ static_cast<Change::What>(i) };

      if (Change::whatName(what) == ignore)
      {
        this->m_ignoredChanges.emplace(what);
        return true;
      }
    }

    return false;
  }

  void Diff::onOptionsChanged()
  {
    this->m_comparePath = this->getOption("compare").second.value_or("");

    if (this->m_comparePath.empty())
    {
      throw std::invalid_argument{ "no comparison file given" };
    }

    if (std::string ignoreList{ this->getOption("ignore").second.value_or("") }; !ignoreList.empty())
    {
      std::size_t start{ 0 };
      bool addedLast{ false };

      while (start < ignoreList.size())
      {
        auto found{ ignoreList.find(',', start) };

        if (found == std::string::npos)
        {
          if (!addedLast)
          {
            addedLast = true;
            found = ignoreList.size();
          }
        }

        const auto ignore{ std::string_view{ ignoreList }.substr(start, found - start) };

        if (!this->addToIgnoreList(ignore))
        {
          throw std::invalid_argument{ "no such change type: " + std::string{ ignore } };
        }

        start = (found + 1);
      }
    }
  }

  ClientStructHolder Diff::loadCompareFile() const
  {
    const td_fluorine::FileSource file{ this->m_comparePath };

    if (file.isOpen())
    {
      return td_fluorine::converter::IntermediateRepresentation::parse(file);
    }
    else
    {
      throw std::runtime_error{ "could open \"" + this->m_comparePath.string() + "\" for comparison" };
    }
  }

  std::vector<std::unique_ptr<Diff::Change>> Diff::diff(const ClientStructHolder& from, const ClientStructHolder& to) const
  {
    std::vector<std::unique_ptr<Change>> changes{};

    const bool ignoreStructRemoved{ this->m_ignoredChanges.contains(Change::What::structRemoved) };
    const bool ignoreVariableRemoved{ this->m_ignoredChanges.contains(Change::What::variableRemoved) };
    const bool ignoreVariableMoved{ this->m_ignoredChanges.contains(Change::What::variableMoved) };
    const bool ignoreVariableTypeChanged{ this->m_ignoredChanges.contains(Change::What::variableTypeChanged) };
    const bool ignoreArraySizeChanged{ this->m_ignoredChanges.contains(Change::What::arraySizeChanged) };

    for (const auto& clientStruct : from)
    {
      if (const auto* toClientStruct{ to.get_optional(clientStruct.name()) })
      {
        for (const auto& clientVariable : clientStruct.variables())
        {
          if (const auto* toClientVariable{ toClientStruct->get_optional(clientVariable.name()) })
          {
            if (!ignoreVariableMoved)
            {
              if (clientVariable.offset() != toClientVariable->offset())
              {
                // TODO: This goes nuts when a variable was added or removed,
                // because all variables after that changed their offset.
                changes.emplace_back(std::make_unique<ChangeVariableMoved>(clientStruct.name(), clientVariable.name(), clientVariable.offset(), toClientVariable->offset()));
              }
            }

            if (!ignoreVariableTypeChanged)
            {
              if (clientVariable.type() != toClientVariable->type())
              {
                changes.emplace_back(std::make_unique<ChangeVariableType>(clientStruct.name(), clientVariable.name(), clientVariable.type(), toClientVariable->type()));
              }
            }

            if (!ignoreArraySizeChanged)
            {
              if (clientVariable.isArray() && toClientVariable->isArray())
              {
                if (clientVariable.elements() != toClientVariable->elements())
                {
                  changes.emplace_back(std::make_unique<ChangeArraySize>(clientStruct.name(), clientVariable.name(), clientVariable.elements(), toClientVariable->elements()));
                }
              }
              else
              {
                // We could do something here too
              }
            }
          }
          else
          {
            if (!ignoreVariableRemoved)
            {
              changes.emplace_back(std::make_unique<ChangeVariableRemoved>(clientStruct.name(), clientVariable.name()));
            }
          }
        }
      }
      else
      {
        if (!ignoreStructRemoved)
        {
          changes.emplace_back(std::make_unique<ChangeStructRemoved>(clientStruct.name()));
        }
      }
    }

    // Almost the same thing, other direction

    const bool ignoreStructAdded{ this->m_ignoredChanges.contains(Change::What::structAdded) };
    const bool ignoreVariableAdded{ this->m_ignoredChanges.contains(Change::What::variableAdded) };

    for (const auto& clientStruct : to)
    {
      if (const auto* fromClientStruct{ from.get_optional(clientStruct.name()) })
      {
        if (!ignoreVariableAdded)
        {
          for (const auto& clientVariable : clientStruct.variables())
          {
            if (!fromClientStruct->hasMember(clientVariable.name()))
            {
              changes.emplace_back(std::make_unique<ChangeVariableAdded>(clientStruct.name(), clientVariable.name()));
            }
          }
        }
      }
      else
      {
        if (!ignoreStructAdded)
        {
          changes.emplace_back(std::make_unique<ChangeStructAdded>(clientStruct.name()));
        }
      }
    }

    return changes;
  }

  nlohmann::json Diff::diffToJson(const diff_type& diff)
  {
    nlohmann::json changes(diff.size(), 0);

    std::ranges::transform(diff, changes.begin(), [](const auto& change) {
      return change->toJson();
    });

    return changes;
  }

  void Diff::dump(const ClientStructHolder& clientStructs)
  {
    assert(this->m_out != nullptr);

    const ClientStructHolder compare{ this->loadCompareFile() };

    const auto changes{ this->diff(clientStructs, compare) };

    const nlohmann::json diff(this->diffToJson(changes));

    *this->m_out << diff.dump(2);
  }
}