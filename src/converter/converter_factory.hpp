#if !defined(HPPtd_fluorine_converter_factory)
#define HPPtd_fluorine_converter_factory

#include "converter.hpp"

#include <memory>

namespace td_fluorine::converter
{
  class ConverterFactory
  {
  public:
    /**
     * Creates the converter for the given name.
     * @param name Short name of the language for which to create a converter.
     * @return The converter with the given name, or a nullptr.
     */
    static std::unique_ptr<Converter> makeConverter(std::string_view name);
  };
}

#endif