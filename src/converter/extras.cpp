#include "extras.hpp"

#include <SourceSDK/public/datamap.hpp>

#include <algorithm>

namespace td_fluorine::converter
{
  void Extras::insertAfter(
      ClientStructHolder& clientStructs,
      std::string_view structName,
      std::string_view variableName,
      ClientVariable&& variable) const
  {
    if (auto foundStruct{ clientStructs.get_optional(std::string{structName}) })
    {
      if (auto foundVariable{ std::ranges::find_if(foundStruct->variables(), [variableName](const auto& clientVariable) {
            return (clientVariable.name() == variableName);
          }) };
          foundVariable != cend(foundStruct->variables()))
      {
        variable.offset(foundVariable->offset() + foundVariable->size(clientStructs));
        foundStruct->addVariable(variable);
      }
    }
  }

  void Extras::dump(const ClientStructHolder& inClientStructs)
  {
    ClientStructHolder clientStructs{ inClientStructs };

    this->insertAfter(clientStructs, "C_BaseEntity", "m_nWaterType", ClientVariable{ "m_bDormant", source_sdk::fieldtype_t::FIELD_BOOLEAN, 0 });

    this->IntermediateRepresentation::dump(clientStructs);
  }
}