#include "cpp.hpp"

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <list>
#include <map>
#include <memory_resource>
#include <set>
#include <utility>

namespace td_fluorine::converter
{
  void CPP::buildVariableTypeMap()
  {
    this->m_variableTypes = {
      { source_sdk::fieldtype_t::FIELD_VOID, { "void*", {} } },
      { source_sdk::fieldtype_t::FIELD_FLOAT, { "float", {} } },
      { source_sdk::fieldtype_t::FIELD_STRING, { this->makeSdkMember("string_t"), this->makeSdkInclude("public/string_t") } },
      { source_sdk::fieldtype_t::FIELD_VECTOR, { this->makeSdkMember("Vector"), this->makeSdkInclude("public/mathlib/vector") } },
      { source_sdk::fieldtype_t::FIELD_QUATERNION, { "Quaternion", this->makeSdkInclude("public/mathlib/vector") } },
      { source_sdk::fieldtype_t::FIELD_INTEGER, { "int", {} } },
      { source_sdk::fieldtype_t::FIELD_BOOLEAN, { "bool", {} } },
      { source_sdk::fieldtype_t::FIELD_SHORT, { "short", {} } },
      { source_sdk::fieldtype_t::FIELD_CHARACTER, { "char", {} } },
      { source_sdk::fieldtype_t::FIELD_COLOR32, { this->makeSdkMember("color32"), this->makeSdkInclude("public/tier0/basetypes") } },
      { source_sdk::fieldtype_t::FIELD_EMBEDDED, { "FIELD_EMBEDDED", {} } }, // should never happen
      { source_sdk::fieldtype_t::FIELD_CUSTOM, { "void*", {} } },
      { source_sdk::fieldtype_t::FIELD_CLASSPTR, { this->makeSdkMember("CBaseEntity") + '*', {} } },
      { source_sdk::fieldtype_t::FIELD_EHANDLE, { this->makeSdkMember("CBaseHandle"), this->makeSdkInclude("public/basehandle") } },
      { source_sdk::fieldtype_t::FIELD_EDICT, { this->makeSdkMember("edict_t") + '*', this->makeSdkInclude("public/edict") } },
      { source_sdk::fieldtype_t::FIELD_POSITION_VECTOR, { this->makeSdkMember("Vector"), this->makeSdkInclude("public/mathlib/vector") } },
      { source_sdk::fieldtype_t::FIELD_TIME, { "float", {} } },
      { source_sdk::fieldtype_t::FIELD_TICK, { "int", {} } },
      { source_sdk::fieldtype_t::FIELD_MODELNAME, { this->makeSdkMember("string_t"), {} } },
      { source_sdk::fieldtype_t::FIELD_SOUNDNAME, { this->makeSdkMember("string_t"), {} } },
      { source_sdk::fieldtype_t::FIELD_INPUT, { "int", {} } }, // This is incorrect, but the size is right.
      { source_sdk::fieldtype_t::FIELD_FUNCTION, { "void*", {} } },
      { source_sdk::fieldtype_t::FIELD_VMATRIX, { this->makeSdkMember("VMatrix"), this->makeSdkInclude("public/mathlib/vmatrix") } },
      { source_sdk::fieldtype_t::FIELD_VMATRIX_WORLDSPACE, { this->makeSdkMember("VMatrix"), {} } },
      { source_sdk::fieldtype_t::FIELD_MATRIX3X4_WORLDSPACE, { this->makeSdkMember("matrix3x4_t"), this->makeSdkInclude("public/mathlib/mathlib") } },
      { source_sdk::fieldtype_t::FIELD_INTERVAL, { this->makeSdkMember("interval_t"), this->makeSdkInclude("public/tier0/basetypes") } },
      { source_sdk::fieldtype_t::FIELD_MODELINDEX, { "int" } },
      { source_sdk::fieldtype_t::FIELD_MATERIALINDEX, { "int" } },
      { source_sdk::fieldtype_t::FIELD_VECTOR2D, { this->makeSdkMember("Vector2D"), this->makeSdkInclude("public/mathlib/vector2d") } },
      { source_sdk::fieldtype_t::FIELD_INTEGER64, { "std::int64_t", "<cstdint>" } },
      { source_sdk::fieldtype_t::FIELD_VECTOR4D, { this->makeSdkMember("Vector4D"), this->makeSdkInclude("public/mathlib/vector4d") } },
      { source_sdk::fieldtype_t::FIELD_UTLVECTOR, { this->makeSdkMember("CUtlVector"), this->makeSdkInclude("public/tier1/utlvector") } }
    };

    assert(size(this->m_variableTypes) == static_cast<std::size_t>(source_sdk::fieldtype_t::FIELD_TYPECOUNT));
  }

  void CPP::ignoreVariables(const ClientStructHolder& clientStructs)
  {
    for (const auto& clientStruct : clientStructs)
    {
      std::size_t nextFreeOffset{ 0 };

      for (const auto& clientVariable : clientStruct.variables())
      {
        // True if this variable would overlap with the previous variable.
        bool overlapping{ clientVariable.offset() < nextFreeOffset };

        if (overlapping)
        {
          this->m_ignoredVariables.emplace(clientStruct.name(), clientVariable.name());
        }
        else
        {
          nextFreeOffset = (clientVariable.offset() + clientVariable.size(clientStructs));
        }
      }
    }
  }

  bool CPP::isIgnoredVariable(const ClientStruct& clientStruct, const ClientVariable& clientVariable) const noexcept
  {
    return this->m_ignoredVariables.contains({ clientStruct.name(), clientVariable.name() });
  }

  std::vector<ClientStruct> CPP::orderStructs(const ClientStructHolder& clientStructs)
  {
    using pair_type = std::pair<std::string_view, std::size_t>;

    auto mr{ std::pmr::monotonic_buffer_resource(clientStructs.size() * sizeof(pair_type)) };
    std::pmr::list<std::string_view> orderedStructs{ &mr };

    for (const auto& clientStruct : clientStructs)
    {
      auto insertAfter{ cend(orderedStructs) };
      decltype(orderedStructs)::difference_type insertAfterIndex{ 0 };

      for (auto dependency : clientStruct.dependencies())
      {
        auto found{ std::ranges::find_if(orderedStructs, [=](const auto& el) { return (el == dependency); }) };

        if (found != cend(orderedStructs))
        {
          auto foundIndex{ std::distance(begin(orderedStructs), found) };

          if (foundIndex >= insertAfterIndex)
          {
            insertAfter = found;
            insertAfterIndex = foundIndex;
          }
        }
      }

      if (insertAfter == cend(orderedStructs))
      {
        orderedStructs.emplace_front(clientStruct.name());
      }
      else
      {
        orderedStructs.emplace(std::next(insertAfter), clientStruct.name());
      }
    }

    assert(size(orderedStructs) == std::size(clientStructs));

    std::vector<ClientStruct> result{};
    result.reserve(std::size(clientStructs));

    for (const auto& el : orderedStructs)
    {
      result.emplace_back(clientStructs.get(std::string{ el }));
    }

    return result;
  }

  std::string CPP::makeSdkMember(std::string_view name) const
  {
    if (this->m_sdkNamespace.empty())
    {
      return std::string{ name };
    }
    else
    {
      return (this->m_sdkNamespace + "::" + std::string{ name });
    }
  }

  std::string CPP::makeSdkInclude(std::string_view path) const
  {
    std::string result{};
    result.reserve(2 + size(path));

    if (this->m_sdkSystemIncludes)
    {
      result += '<';
    }
    else
    {
      result += '"';
    }

    result += this->m_sdkPath + std::string{ path } + '.' + this->m_sdkFileExtension;

    if (this->m_sdkSystemIncludes)
    {
      result += '>';
    }
    else
    {
      result += '"';
    }

    return result;
  }

  void CPP::dumpIncludes(const ClientStructHolder& clientStructs,
                         std::function<std::ostream&()> indented,
                         std::size_t&) const
  {
    std::set<std::string_view> includes{};

    for (const auto& clientStruct : clientStructs)
    {
      for (const auto& clientVariable : clientStruct.variables())
      {
        if (clientVariable.type() == source_sdk::fieldtype_t::FIELD_EMBEDDED)
        {
          // No include required. The type is defined in the dump.
          continue;
        }

        if (auto found{ this->m_variableTypes.find(clientVariable.type()) }; found != cend(this->m_variableTypes))
        {
          if (!found->second.includePath.empty())
          {
            includes.emplace(found->second.includePath);
          }
        }
      }
    }

    for (auto include : includes)
    {
      indented() << "#include " << include << '\n';
    }

    if (this->m_assert)
    {
      // TODO: Update all functions so that we can use `out` instead of
      // `indented`.
      indented() << '\n';
      indented() << "#include <cassert>\n";
      indented() << "#include <cstddef>\n";
    }
  }

  void CPP::dumpVariable(const ClientVariable& clientVariable,
                         std::function<std::ostream&()> indented,
                         std::size_t&) const
  {
    auto& out{ indented() };

    if (clientVariable.embeddedType().empty())
    {
      out << this->m_variableTypes.at(clientVariable.type()).name;
    }
    else
    {
      out << clientVariable.embeddedType();
    }

    out << ' ' << clientVariable.name();

    if (clientVariable.isArray())
    {
      out << '[' << std::dec << clientVariable.elements() << ']';
    }

    out << ';';

    out << " // 0x" << std::hex << clientVariable.offset() << '\n';
  }

  void CPP::dumpStruct(const ClientStructHolder& holder,
                       const ClientStruct& clientStruct,
                       std::function<std::ostream&()> indented,
                       std::size_t& indentionLevel) const
  {
    std::size_t parentSize{ 0 };

    if (!clientStruct.parentName().empty())
    {
      const auto& parent{ holder.get(clientStruct.parentName()) };
      parentSize = parent.size(holder);
    }

    auto& out{ indented() };

    out << "struct " << clientStruct.name();

    if (!clientStruct.parentName().empty())
    {
      out << " : " << clientStruct.parentName();
    }

    out << '\n';

    indented() << "{\n";
    ++indentionLevel;

    // Smallest possible offset of the next variable
    std::size_t nextFreeOffset{ parentSize };

    for (const auto& clientVariable : clientStruct.variables())
    {
      // True if this variable would overlap with the previous variable.
      bool ignoreVariable{ this->isIgnoredVariable(clientStruct, clientVariable) };
      auto oldIndentionLevel{ indentionLevel };

      if (ignoreVariable)
      {
        // If this is the last member, add a pad to preserve the size.
        if (&clientVariable == &*clientStruct.variables().crbegin())
        {
          auto oldStructSize{ clientStruct.size(holder) };
          auto sizeWithoutVariable{ nextFreeOffset };
          auto padSize{ oldStructSize - sizeWithoutVariable };

          indented() << "char _pad_" << clientVariable.name() << "[0x" << padSize << "];\n";
        }

        // A warning was already printed by the dumper.
        indented() << "// "; // comment out the variable.
        indentionLevel = 0;
      }
      else
      {
        if (auto padSize{ clientVariable.offset() - nextFreeOffset })
        {
          out << std::hex;
          indented() << "char _pad_" << nextFreeOffset << "[0x" << padSize << "];\n";
        }
      }

      this->dumpVariable(clientVariable, indented, indentionLevel);

      indentionLevel = oldIndentionLevel;

      if (!ignoreVariable)
      {
        nextFreeOffset = (clientVariable.offset() + clientVariable.size(holder));
      }
    }

    --indentionLevel;
    indented() << "}; // 0x" << clientStruct.size(holder) << '\n';
  }

  void CPP::dumpStructs(std::span<const ClientStruct> clientStructs,
                        const ClientStructHolder& holder,
                        std::function<std::ostream&()> indented,
                        std::size_t& indentionLevel) const
  {
    auto originalIndentionLevel{ indentionLevel };
    indentionLevel = 0;
    auto& out{ indented() };
    indentionLevel = originalIndentionLevel;

    for (const auto& clientStruct : clientStructs)
    {
      bool isLastStruct{ &clientStruct == &clientStructs.back() };

      this->dumpStruct(holder, clientStruct, indented, indentionLevel);

      if (!isLastStruct)
      {
        out << '\n';
      }
    }
  }

  void CPP::dumpAssertions(const ClientStructHolder& clientStructs,
                           std::function<std::ostream&()> indented,
                           std::size_t&) const
  {
    for (const auto& clientStruct : clientStructs)
    {
      if (clientStruct.parentName().empty())
      {
        for (const auto& clientVariable : clientStruct.variables())
        {
          bool ignoreVariable{ this->isIgnoredVariable(clientStruct, clientVariable) };
          indented() << ((ignoreVariable) ? "// " : "") << "static_assert(offsetof(" << clientStruct.name() << ", " << clientVariable.name() << ") == " << std::hex << "0x" << clientVariable.offset() << ");\n";
        }
      }
      else
      {
        indented() << "// " << clientStruct.name() << " doesn't have standard layout\n";
      }
    }

    indented() << '\n';
  }

  void CPP::onOptionsChanged()
  {
    this->m_namespace = this->getOption("namespace").second.value_or("");
    this->m_headerGuardName = this->getOption("header-guard").second.value_or("HPP_SOURCE_SDK_NETVAR_DUMP");
    this->m_sdkPath = this->getOption("sdk-path").second.value_or("");
    if (!this->m_sdkPath.empty() && !this->m_sdkPath.ends_with('/'))
    {
      this->m_sdkPath += '/';
    }
    this->m_sdkFileExtension = this->getOption("sdk-header-extension").second.value_or("h");
    this->m_sdkNamespace = this->getOption("sdk-namespace").second.value_or("");
    this->m_sdkSystemIncludes = !this->getOption("sdk-no-system").first;
    this->m_assert = !this->getOption("no-assert").first;
    this->m_align = !this->getOption("no-align").first;

    this->buildVariableTypeMap();
  }

  void CPP::dump(const ClientStructHolder& clientStructs)
  {
    assert(this->m_out != nullptr);

    auto& out{ *this->m_out };

    auto indent{ [&out](std::size_t level) -> std::ostream& {
      constexpr std::string_view space{ "  " };

      for (std::size_t i{ 0 }; i < level; ++i)
      {
        out << space;
      }

      return out;
    } };

    std::size_t indentionLevel{ 0 };

    auto indented{
      [&]() -> std::ostream& {
        return indent(indentionLevel);
      }
    };

    this->ignoreVariables(clientStructs);
    auto orderedStructs{ this->orderStructs(clientStructs) };

    indented() << "#if !defined(" << this->m_headerGuardName << ")\n";
    indented() << "#define " << this->m_headerGuardName << '\n';
    indented() << '\n';

    for (auto line : this->m_metadataMessage)
    {
      indented() << "// " << line << '\n';
    }

    indented() << '\n';

    this->dumpIncludes(clientStructs, indented, indentionLevel);

    out << '\n';

    if (!this->m_namespace.empty())
    {
      indented() << "namespace " << this->m_namespace << '\n';
      indented() << "{\n";
      ++indentionLevel;
    }

    if (this->m_align)
    {
      out << "#pragma pack(push, 1)\n\n";
    }

    this->dumpStructs(orderedStructs, clientStructs, indented, indentionLevel);

    out << '\n';

    if (this->m_align)
    {
      out << "#pragma pack(pop)\n\n";
    }

    if (this->m_assert)
    {
      this->dumpAssertions(clientStructs, indented, indentionLevel);
    }

    if (!this->m_namespace.empty())
    {
      --indentionLevel;
      indented() << "}\n";
      indented() << '\n';
    }

    out << "#endif";
  }
}