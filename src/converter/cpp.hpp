#if !defined(HPPtd_fluorine_cpp)
#define HPPtd_fluorine_cpp

#include "converter.hpp"

#include "client/client_struct.hpp"
#include "client/client_struct_holder.hpp"
#include "client/client_variable.hpp"

#include <SourceSDK/public/datamap.hpp>

#include <map>
#include <set>
#include <span>
#include <string>
#include <string_view>

namespace td_fluorine::converter
{
  class CPP : public Converter
  {
  public:
    using Converter::Converter;

    static constexpr std::string_view name{ "cpp" };

  private:
    /**
     * Holds C++ information about a variable type.
     */
    struct VariableType
    {
      /**
       * Identifier, including the namespace.
       */
      std::string name{};

      /**
       * Path to the file this type is defined in, or an empty string.
       */
      std::string includePath{};
    };

  private:
    /**
     *
     */
    std::map<source_sdk::fieldtype_t, VariableType> m_variableTypes{};

    /**
     * Variables that are not dumped, eg. because they are overlapping.
     * The pair consists of the struct name and variable name.
     */
    std::set<std::pair<std::string_view, std::string_view>> m_ignoredVariables{};

    /**
     * Name of the macro used as the header guard.
     */
    std::string m_headerGuardName{};

    /**
     * Name of the namespace to dump into.
     * Or an empty string.
     */
    std::string m_namespace{};

    /**
     * Path to the user's source sdk.
     */
    std::string m_sdkPath{};

    /**
     * Name of the user's source sdk namespace.
     * Or an empty string.
     */
    std::string m_sdkNamespace{};

    /**
     * Extension to use for source sdk files.
     */
    std::string m_sdkFileExtension{};

    /**
     * Use system includes (<>) for the user's sdk.
     */
    bool m_sdkSystemIncludes{};

    /**
     * Output assertions.
     */
    bool m_assert{};

    /**
     * Output an alignment pragma.
     */
    bool m_align{};

  private:
    /**
     * Populates {@link m_variableTypes}.
     */
    void buildVariableTypeMap();

    /**
     * Collects variables that should be ignored.
     * Populates {@link m_ignoredVariables}.
     */
    void ignoreVariables(const ClientStructHolder& clientStructs);

    /**
     * @return True if the variable is in the list of ignored variables.
     */
    [[nodiscard]] bool isIgnoredVariable(const ClientStruct& clientStruct, const ClientVariable& clientVariable) const noexcept;

    /**
     * Reorders structs such that structs have their dependencies fulfilled
     * before being defined. Dependencies are inheritance and member variables
     * of struct type.
     * Invalidates all references held into @clientStructs, including the data
     * pointer itself.
     * @param clientStructs The structs.
     * @return The ordered structs.
     */
    std::vector<ClientStruct> orderStructs(const ClientStructHolder& clientStructs);

    /**
     * Adds the source sdk namespace selector before the given name, if a source
     * sdk namespace is set.
     * @param name Name to move into the source sdk namespace.
     * @return @name in the source sdk, or unchanged if no sdk namespace was
     *         set.
     */
    [[nodiscard]] std::string makeSdkMember(std::string_view name) const;

    /**
     * Turns the given path into an include into the user's source sdk.
     * @param path Path the the sdk file.
     * @return Path including quotation marks or brackets.
     */
    [[nodiscard]] std::string makeSdkInclude(std::string_view path) const;

    /**
     * Collects and prints all required includes.
     * @param clientStructs All client structs. Used for size calculation.
     * @param indented A function that prints indention and returns the output
     *                 stream.
     * @param indentionLevel Level of indention.
     */
    void dumpIncludes(const ClientStructHolder& clientStructs,
                      std::function<std::ostream&()> indented,
                      std::size_t& indentionLevel) const;

    /**
     * Prints a single variable, no padding.
     * @param clientVariable The variable to dump.
     * @param indented A function that prints indention and returns the output
     *                 stream.
     * @param indentionLevel Level of indention.
     */
    void dumpVariable(const ClientVariable& clientVariable,
                      std::function<std::ostream&()> indented,
                      std::size_t& indentionLevel) const;

    /**
     * Prints a single struct.
     * @param clientStructs All client structs. Used for size calculation.
     * @param clientStruct The struct to dump.
     * @param indented A function that prints indention and returns the output
     *                 stream.
     * @param indentionLevel Level of indention.
     */
    void dumpStruct(const ClientStructHolder& holder,
                    const ClientStruct& clientStruct,
                    std::function<std::ostream&()> indented,
                    std::size_t& indentionLevel) const;

    /**
     * Dumps all structs.
     * @param clinetStructs The structs to dump.
     * @param indented A function that prints indention and returns the output
     *                 stream.
     * @param indentionLevel Level of indention.
     */
    void dumpStructs(std::span<const ClientStruct> clientStructs,
                     const ClientStructHolder& holder,
                     std::function<std::ostream&()> indented,
                     std::size_t& indentionLevel) const;

    /**
     * Dumps assertions that verify offsets.
     * @param clinetStructs The structs.
     * @param indented A function that prints indention and returns the output
     *                 stream.
     * @param indentionLevel Level of indention.
     */
    void dumpAssertions(const ClientStructHolder& clientStructs,
                        std::function<std::ostream&()> indented,
                        std::size_t& indentionLevel) const;

  protected:
    /**
     * @override
     */
    void onOptionsChanged() override;

  public:
    /**
     * @override
     */
    void dump(const ClientStructHolder& clientStructs) override;
  };
}

#endif