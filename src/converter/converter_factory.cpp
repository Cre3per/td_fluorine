#include "converter_factory.hpp"

#include "cpp.hpp"
#include "diff.hpp"
#include "extras.hpp"
#include "intermediate_representation.hpp"

#include <functional>

namespace td_fluorine::converter
{
  std::unique_ptr<Converter> ConverterFactory::makeConverter(std::string_view name)
  {
    std::map<std::string_view, std::function<std::unique_ptr<Converter>()>> converters{
      { CPP::name, [] { return std::make_unique<CPP>(); } },
      { Diff::name, [] { return std::make_unique<Diff>(); } },
      { Extras::name, [] { return std::make_unique<Extras>(); } },
      { IntermediateRepresentation::name, [] { return std::make_unique<IntermediateRepresentation>(); } }
    };

    if (auto found{ converters.find(name) }; found != cend(converters))
    {
      return found->second();
    }
    else
    {
      return nullptr;
    }
  }
}
