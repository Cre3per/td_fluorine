#if !defined(HPPtd_fluorine_intermediate_representation)
#define HPPtd_fluorine_intermediate_representation

#include "converter.hpp"

#include "client/client_struct.hpp"
#include "data_source/binary_data_source.hpp"

#include <string>
#include <string_view>
#include <vector>

namespace td_fluorine::converter
{
  class IntermediateRepresentation : public Converter
  {
  public:
    using Converter::Converter;

    static constexpr std::string_view name{ "ir" };

  public:
    /**
     * Extracts client structs from the intermediate representation.
     * Throws on error.
     * @param data The intermediate representation.
     * @return The parsed client structs.
     */
    static ClientStructHolder parse(
        const td_fluorine::BinaryDataSource& data);

  public:
    /**
     * @override
     */
    void dump(const ClientStructHolder& clientStructs) override;
  };
}

#endif