#if !defined(HPPtd_fluorine_converter_extras)
#define HPPtd_fluorine_converter_extras

#include "intermediate_representation.hpp"

#include "client/client_variable.hpp"

#include <string_view>
#include <vector>

namespace td_fluorine::converter
{
  class Extras : public IntermediateRepresentation
  {
  public:
    using IntermediateRepresentation::IntermediateRepresentation;

    static constexpr std::string_view name{ "extra" };

  private:
    /**
     * Inserts @variable after @structName::@variableName
     * @param clientStructs The client structs.
     * @param structName Name of the target struct.
     * @param variableName Name of the variable after which to insert the new variable.
     * @param variable The new variable. Offset will be adjusted.
     * Throws if @structName::@variableName doesn't exist.
     */
    void insertAfter(
        ClientStructHolder& clientStructs,
        std::string_view structName,
        std::string_view variableName,
        ClientVariable&& variable) const;

  public:
    /**
     * @override
     */
    void dump(const ClientStructHolder& clientStructs) override;
  };
}

#endif