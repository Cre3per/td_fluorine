#include "intermediate_representation.hpp"

#include "exception/parse_error.hpp"

#include <algorithm>
#include <cassert>
#include <istream>
#include <iterator>
#include <ranges>
#include <stdexcept>

namespace td_fluorine::converter
{
  /**
   * Welcome to fun town.
   */
  ClientStructHolder IntermediateRepresentation::parse(
      const td_fluorine::BinaryDataSource& data)
  {
    ClientStructHolder result{};
    td_fluorine::BinaryDataSource::size_type index{ 0 };

    auto required{
      [&](auto fn) {
        return [fn, index, &data] {
          if (!fn())
          {
            throw ParseError{ index, static_cast<char>(data.at(index)) };
          }
        };
      }
    };

    auto makeCharacter{
      [&](char ch) {
        return [&, ch] {
          return ((data.at(index) == ch) ? !!++index : false);
        };
      }
    };

    auto makeString{
      [&](std::string_view str) {
        return [&, str] {
          if ((index + size(str)) >= std::size(data))
          {
            return false;
          }
          else
          {
            if (std::equal(cbegin(str), cend(str), std::begin(data) + index, [](auto lhs, auto rhs) { return (lhs == rhs); }))
            {
              index += size(str);
              return true;
            }
            else
            {
              return false;
            }
          }
        };
      }
    };

    auto makeAnyOf{
      [](auto... fn) {
        return [fn...] {
          return (fn() || ...);
        };
      }
    };

    auto makeAnyNumberOf{
      [](auto fn) {
        return [fn] {
          while (fn())
            ;
        };
      }
    };

    auto whiteSpace{
      makeAnyNumberOf(makeAnyOf(makeCharacter(' '), makeCharacter('\n')))
    };

    auto parseString{
      [&](std::string_view str) {
        makeString(str)();
      }
    };

    auto parseStringLiteral{
      [&] {
        auto oldIndex{ index };
        std::string str{};
        required(makeCharacter('"'))();
        while (index < std::size(data))
        {
          char ch{ static_cast<char>(data.at(index++)) };

          if (ch == '"')
          {
            return str;
          }

          str += ch;
        }

        throw ParseError{ oldIndex, "EOF", "\"" };
      }
    };

    auto parseUnsigned{
      [&] {
        auto oldIndex{ index };
        bool hasParsedAtLeastOne{ false };
        std::size_t result{ 0 };

        while (index < std::size(data))
        {
          auto ch{ static_cast<char>(data.at(index)) };

          if ((ch >= '0') && (ch <= '9'))
          {
            result *= 10;
            result += static_cast<std::size_t>(ch - '0');
          }
          else
          {
            break;
          }

          ++index;
          hasParsedAtLeastOne = true;
        }

        if (hasParsedAtLeastOne)
        {
          return result;
        }
        else
        {
          throw ParseError{ oldIndex, static_cast<char>(data.at(index)), "DIGIT" };
        }
      }
    };

    auto parseHexadecimal{
      [&] {
        bool hasParsedAtLeastOne{ false };
        std::size_t result{ 0 };

        required(makeString("0x"))();

        auto oldIndex{ index };

        while (index < std::size(data))
        {
          auto ch{ static_cast<char>(data.at(index)) };

          if ((ch >= '0') && (ch <= '9'))
          {
            result *= 0x10;
            result += static_cast<std::size_t>(ch - '0');
          }
          else if ((ch >= 'a') && (ch <= 'f'))
          {
            result *= 0x10;
            result += (0x0a + static_cast<std::size_t>(ch - 'a'));
          }
          else
          {
            break;
          }

          ++index;
          hasParsedAtLeastOne = true;
        }

        if (hasParsedAtLeastOne)
        {
          return result;
        }
        else
        {
          throw ParseError{ oldIndex, static_cast<char>(data.at(index)), "DIGIT" };
        }
      }
    };

    auto parseClientVariable{
      [&] {
        required(makeCharacter('{'))();
        whiteSpace();

        required(makeString("\"name\":"))();
        whiteSpace();
        std::string name{ parseStringLiteral() };
        required(makeCharacter(','))();

        whiteSpace();

        required(makeString("\"offset\":"))();
        whiteSpace();
        std::size_t offset{ parseHexadecimal() };
        required(makeCharacter(','))();

        whiteSpace();

        required(makeString("\"type\":"))();
        whiteSpace();
        std::string typeName{ parseStringLiteral() };
        required(makeCharacter(','))();

        whiteSpace();

        required(makeString("\"elements\":"))();
        whiteSpace();
        std::size_t elements{ parseUnsigned() };

        whiteSpace();
        required(makeCharacter('}'))();

        auto variableType{ source_sdk::getFieldTypeByName(typeName) };

        ClientVariable clientVariable{ name, variableType.value_or(source_sdk::fieldtype_t::FIELD_VOID), offset };

        clientVariable.elements(elements);

        if (!variableType.has_value())
        {
          clientVariable.embeddedType(typeName);
        }

        return clientVariable;
      }
    };

    auto parseClientStruct{
      [&] {
        required(makeCharacter('{'))();
        whiteSpace();

        required(makeString("\"name\":"))();
        whiteSpace();
        ClientStruct result{ parseStringLiteral() };
        required(makeCharacter(','))();

        whiteSpace();

        if (makeString("\"parent\":")())
        {
          whiteSpace();
          result.parentName(parseStringLiteral());
          required(makeCharacter(','))();

          whiteSpace();
        }

        parseString("\"variables\":");
        whiteSpace();
        required(makeCharacter('['))();

        while (true)
        {
          whiteSpace();
          if (data.at(index) == '{')
          {
            result.addVariable(parseClientVariable());
            whiteSpace();

            if (!makeCharacter(',')())
            {
              break;
            }
          }
          else
          {
            // empty variable list
            break;
          }
        }

        whiteSpace();
        required(makeCharacter(']'))();
        whiteSpace();
        required(makeCharacter('}'))();

        return result;
      }
    };

    makeCharacter('[')();
    whiteSpace();

    while (index < std::size(data))
    {
      auto clientStruct{ parseClientStruct() };

      result.insert(std::move(clientStruct));

      if (!makeCharacter(',')())
      {
        break;
      }

      whiteSpace();
    }

    whiteSpace();
    required(makeCharacter(']'))();

    return result;
  }

  void IntermediateRepresentation::dump(const ClientStructHolder& clientStructs)
  {
    assert(this->m_out != nullptr);

    auto& out{ *this->m_out };

    auto indent{ [&out](std::size_t level) -> decltype(out) {
      constexpr std::string_view space{ "  " };

      for (std::size_t i{ 0 }; i < level; ++i)
      {
        out << space;
      }

      return out;
    } };

    out << "[\n";

    for (const auto& clientStruct : clientStructs)
    {
      bool isLastStruct{ &clientStruct == &clientStructs.back() };

      indent(1) << "{\n";

      indent(2) << "\"name\": \"" << clientStruct.name() << "\",\n";

      if (!clientStruct.parentName().empty())
      {
        indent(2) << "\"parent\": \"" << clientStruct.parentName() << "\",\n";
      }

      indent(2) << "\"variables\": [\n";

      for (const auto& clientVariable : clientStruct.variables())
      {
        bool isLastVariable{ &clientVariable == &*std::prev(cend(clientStruct.variables())) };

        indent(3) << "{\n";
        indent(4) << "\"name\": \"" << clientVariable.name() << "\",\n";
        indent(4) << "\"offset\": 0x" << std::hex << clientVariable.offset() << ",\n";
        indent(4) << "\"type\": \"";
        if (clientVariable.embeddedType().empty())
        {
          out << clientVariable.type();
        }
        else
        {
          out << clientVariable.embeddedType();
        }
        out << "\",\n";
        indent(4) << "\"elements\": " << std::dec << clientVariable.elements() << "\n";
        indent(3) << "}";

        if (!isLastVariable)
        {
          out << ',';
        }

        out << '\n';
      }

      indent(2) << "]\n";

      indent(1) << '}';

      if (!isLastStruct)
      {
        out << ',';
      }

      out << '\n';
    }

    out << ']';
  }
}