#include "converter.hpp"

#include <algorithm>

namespace td_fluorine::converter
{
  std::pair<bool, std::optional<std::string>>
  Converter::getOption(std::string_view name) const
  {
    if (auto found{ this->m_options.find(data(name)) }; found != this->m_options.cend())
    {
      return { true, found->second };
    }

    return { false, {} };
  }

  void Converter::setOptions(const std::map<std::string, std::optional<std::string>>& options) noexcept
  {
    this->m_options = options;
    this->onOptionsChanged();
  }

  void Converter::setMetadataMessage(std::span<const std::string> lines)
  {
    this->m_metadataMessage.resize(size(lines));
    std::ranges::copy(lines, this->m_metadataMessage.begin());
  }

  void Converter::setOutput(std::ostream& stream,
                            const std::filesystem::path& directory) noexcept
  {
    this->m_out = &stream;
    this->m_path = directory;
  }
}