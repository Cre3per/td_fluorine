#if !defined(HPPtd_fluorine_diff)
#define HPPtd_fluorine_diff

#include "converter.hpp"

#include <SourceSDK/public/datamap.hpp>

#include <array>
#include <cstddef>
#include <filesystem>
#include <memory>
#include <nlohmann/json.hpp>
#include <set>
#include <sstream>
#include <string_view>
#include <vector>

namespace td_fluorine::converter
{
  class Diff : public Converter
  {
  private:
    class Change
    {
    public:
      enum class What
      {
        structAdded,
        structRemoved,

        variableAdded,
        variableRemoved,
        variableMoved,
        variableTypeChanged,
        arraySizeChanged,

        count
      };

      [[nodiscard]] static constexpr std::string_view whatName(What what)
      {
        constexpr std::array names{
          "structAdded",
          "structRemoved",

          "variableAdded",
          "variableRemoved",
          "variableMoved",
          "variableTypeChanged",
          "arraySizeChanged"
        };

        static_assert(names.size() == static_cast<std::size_t>(What::count));

        return names.at(static_cast<std::size_t>(what));
      }

    private:
      What m_what{};

    public:
      virtual nlohmann::json toJson()
      {
        return nlohmann::json(
            { { "what", whatName(this->m_what) } });
      }

      constexpr explicit Change(What what) noexcept
          : m_what{ what }
      {
      }

      virtual ~Change() = default;
    };

    class ChangeStructAdded : public Change
    {
    private:
      std::string_view m_name{};

    public:
      nlohmann::json toJson() override
      {
        auto result(this->Change::toJson());
        result["name"] = this->m_name;
        return result;
      }

      constexpr explicit ChangeStructAdded(std::string_view name) noexcept
          : Change{ What::structAdded },
            m_name{ name }
      {
      }
    };

    class ChangeStructRemoved : public Change
    {
    private:
      std::string_view m_name{};

    public:
      nlohmann::json toJson() override
      {
        auto result(this->Change::toJson());
        result["name"] = this->m_name;
        return result;
      }

      constexpr explicit ChangeStructRemoved(std::string_view name) noexcept
          : Change{ What::structRemoved },
            m_name{ name }
      {
      }
    };

    class ChangeVariableAdded : public Change
    {
    private:
      std::string_view m_structName{};
      std::string_view m_name{};

    public:
      nlohmann::json toJson() override
      {
        auto result(this->Change::toJson());
        result["struct"] = this->m_structName;
        result["name"] = this->m_name;
        return result;
      }

      constexpr ChangeVariableAdded(std::string_view structName, std::string_view name) noexcept
          : Change{ What::variableAdded },
            m_structName{ structName },
            m_name{ name }
      {
      }
    };

    class ChangeVariableRemoved : public Change
    {
    private:
      std::string_view m_structName{};
      std::string_view m_name{};

    public:
      nlohmann::json toJson() override
      {
        auto result(this->Change::toJson());
        result["struct"] = this->m_structName;
        result["name"] = this->m_name;
        return result;
      }

      constexpr ChangeVariableRemoved(std::string_view structName, std::string_view name) noexcept
          : Change{ What::variableRemoved },
            m_structName{ structName },
            m_name{ name }
      {
      }
    };

    class ChangeVariableMoved : public Change
    {
    private:
      std::string_view m_structName{};
      std::string_view m_name{};
      std::size_t m_oldOffset{};
      std::size_t m_newOffset{};

    public:
      nlohmann::json toJson() override
      {
        auto result(this->Change::toJson());
        result["struct"] = this->m_structName;
        result["name"] = this->m_name;
        result["from"] = this->m_oldOffset;
        result["to"] = this->m_newOffset;

        return result;
      }

      constexpr ChangeVariableMoved(std::string_view structName, std::string_view name, std::size_t oldOffset, std::size_t newOffset) noexcept
          : Change{ What::variableMoved },
            m_structName{ structName },
            m_name{ name },
            m_oldOffset{ oldOffset },
            m_newOffset{ newOffset }
      {
      }
    };

    class ChangeVariableType : public Change
    {
    private:
      std::string_view m_structName{};
      std::string_view m_name{};
      source_sdk::fieldtype_t m_oldType{};
      source_sdk::fieldtype_t m_newType{};

    public:
      nlohmann::json toJson() override
      {
        auto result(this->Change::toJson());
        result["struct"] = this->m_structName;
        result["name"] = this->m_name;
        std::stringstream ss{};
        ss << this->m_oldType;
        result["from"] = ss.str();
        ss = {};
        ss << this->m_newType;
        result["to"] = ss.str();

        return result;
      }

      constexpr ChangeVariableType(std::string_view structName, std::string_view name, source_sdk::fieldtype_t oldType, source_sdk::fieldtype_t newType) noexcept
          : Change{ What::variableTypeChanged },
            m_structName{ structName },
            m_name{ name },
            m_oldType{ oldType },
            m_newType{ newType }
      {
      }
    };

    class ChangeArraySize : public Change
    {
    private:
      std::string_view m_structName{};
      std::string_view m_name{};
      std::size_t m_oldSize{};
      std::size_t m_newSize{};

    public:
      nlohmann::json toJson() override
      {
        auto result(this->Change::toJson());
        result["struct"] = this->m_structName;
        result["name"] = this->m_name;
        result["from"] = this->m_oldSize;
        result["to"] = this->m_newSize;

        return result;
      }

      constexpr ChangeArraySize(std::string_view structName, std::string_view name, std::size_t oldSize, std::size_t newSize)
          : Change{ What::arraySizeChanged },
            m_structName{ structName },
            m_name{ name },
            m_oldSize{ oldSize },
            m_newSize{ newSize }
      {
      }
    };

    using diff_type = std::vector<std::unique_ptr<Change>>;

  public:
    using Converter::Converter;

    static constexpr std::string_view name{ "diff" };

  private:
    /**
     * Path of the IR file to compare to.
     */
    std::filesystem::path m_comparePath{};

    /**
     * Change types that are ignored.
     */
    std::set<Change::What> m_ignoredChanges{};

  private:
    /**
     * Loads {@link m_comparePath}.
     */
    [[nodiscard]] ClientStructHolder loadCompareFile() const;

    /**
     * Generates a diff.
     * @from and @to are not commutative
     * @param from Old dump
     * @param to New dump
     */
    [[nodiscard]] diff_type diff(
        const ClientStructHolder& from,
        const ClientStructHolder& to) const;

    [[nodiscard]] static nlohmann::json diffToJson(const diff_type& diff);

    /**
     * Adds a filter to {@link m_ignoredChages}.
     * @param ignore Name of the change type. Same values as those in
     * {@link Change::whatName}.
     * @return False if @ignore is invalid.
     */
    bool addToIgnoreList(std::string_view ignore);

  protected:
    /**
     * @override
     */
    void onOptionsChanged() override;

  public:
    /**
     * @override
     */
    void dump(const ClientStructHolder& clientStructs) override;
  };
}

#endif