#if !defined(HPPtd_fluorine_converter)
#define HPPtd_fluorine_converter

#include "client/client_struct_holder.hpp"

#include <array>
#include <filesystem>
#include <functional>
#include <map>
#include <optional>
#include <ostream>
#include <string_view>
#include <utility>
#include <vector>

namespace td_fluorine::converter
{
  class Converter
  {
  private:
    /**
     * Command line options.
     * Names are without prefixes.
     */
    std::map<std::string, std::optional<std::string>> m_options{};

  protected:
    /**
     * Output stream. Used exclusively for converted dumps.
     */
    std::ostream* m_out{};

    /**
     * Directory in which the file pointed to by {@link m_out} lives. Used by
     * converters that output multiple files.
     */
    std::filesystem::path m_path{};

    /**
     *
     */
    std::vector<std::string> m_metadataMessage{};

  protected:
    /**
     * Gets a command line option.
     * @param name Name of the command line option without prefixes.
                   Must be zero-terminated.
     * @return Whether or not the option is set, and optionally the value.
     */
    [[nodiscard]] std::pair<bool, std::optional<std::string>> getOption(std::string_view name) const;

    /**
     * Called whenever the options change.
     */
    virtual void onOptionsChanged(){};

  public:
    /**
     * @param options Command line options. Must outlive the {@link dump} call.
     */
    void setOptions(const std::map<std::string, std::optional<std::string>>& options) noexcept;

    /**
     * Sets the metadata message to be included as a comment in the output.
     */
    void setMetadataMessage(std::span<const std::string> lines);

    /**
     * @param stream The stream to write the dump to.
     * @param directory Path to the directory in which the file pointed to by
     *                  @stream lives. This is used by converters that write
     *                  multiple files.
     */
    void setOutput(std::ostream& stream,
                   const std::filesystem::path& directory) noexcept;

    /**
     * Dumps all given client structs.
     * @param clientStructs The client structs to dump.
     */
    virtual void dump(const ClientStructHolder& clientStructs) = 0;

  public:
    /**
     *
     */
    virtual ~Converter() = default;
  };
}

#endif