#include "file_source.hpp"

#include <fstream>

namespace td_fluorine
{
  FileSource::size_type FileSource::size() const noexcept
  {
    return std::size(this->m_data);
  }

  FileSource::byte_type FileSource::at(size_type index) const
  {
    return this->m_data.at(this->redirect(index));
  }

  FileSource::byte_type FileSource::operator[](size_type index) const noexcept
  {
    return this->m_data[this->redirect(index)];
  }

  const FileSource::byte_type* FileSource::begin() const noexcept
  {
    return data(this->m_data);
  }

  const FileSource::byte_type* FileSource::end() const noexcept
  {
    return (data(this->m_data) + std::size(this->m_data));
  }

  bool FileSource::isOpen() const noexcept
  {
    return this->m_isOpen;
  }
  
  FileSource::FileSource(const std::filesystem::path& path)
  {
    std::ifstream ifs{ path, std::ios::binary | std::ios::ate };

    if ((m_isOpen = ifs.is_open()))
    {
      auto size{ ifs.tellg() };
      ifs.seekg(0);

      auto buf{ std::make_unique<char[]>(static_cast<std::size_t>(size)) };

      ifs.read(buf.get(), size);

      this->m_data = value_type(buf.get(), buf.get() + size);
    }
  }
}