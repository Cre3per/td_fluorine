#if !defined(HPPtd_fluorine_file_source)
#define HPPtd_fluorine_file_source

#include "binary_data_source.hpp"

#include <filesystem>
#include <vector>

namespace td_fluorine
{
  /**
   * Loads an entire file into memory. Doesn't keep the file open.
   */
  class FileSource : public BinaryDataSource
  {
  public:
    using value_type = std::vector<byte_type>;

  private:
    /**
     * Binary data.
     */
    value_type m_data{};

  protected:
    /**
     * True if the last file read was successful.
     */
    bool m_isOpen{ false };

  public:
    /**
     * @override
     */
    [[nodiscard]] size_type size() const noexcept override;

    /**
     * @override
     */
    [[nodiscard]] byte_type at(size_type index) const override;

    /**
     * @override
     */
    [[nodiscard]] byte_type operator[](size_type index) const noexcept override;

    /**
     * @override
     */
    [[nodiscard]] const byte_type* begin() const noexcept override;

    /**
     * @override
     */
    [[nodiscard]] const byte_type* end() const noexcept override;

    /**
     * @return True if the last file read was successful.
     */
    [[nodiscard]] bool isOpen() const noexcept;

  public:
    /**
     * Loads the file from the file system.
     * Call {@link isOpen} to check for success.
     * @param path The path to the file.
     */
    FileSource(const std::filesystem::path& path);
  };
}

#endif