#include "elf64_data_source.hpp"

#include <elf.h>

#include <cstdint>
#include <iostream>

namespace td_fluorine
{
  Elf64DataSource::size_type
  Elf64DataSource::redirect(Elf64DataSource::size_type offset) const
  {
    if ((offset >= this->m_mappedData.virtualAddressBegin) &&
        (offset < this->m_mappedData.virtualAddressEnd))
    {
      offset = (this->m_mappedData.offset +
                (offset - this->m_mappedData.virtualAddressBegin));
    }

    return offset;
  }

  std::optional<Elf64_Shdr>
  Elf64DataSource::findSection(std::string_view name) const noexcept
  {
    auto header{ this->readRaw<Elf64_Ehdr>(0) };

    std::vector<std::pair<size_type, size_type>> result{};

    auto stringTableHeader{ this->readRaw<Elf64_Shdr>(
        header.e_shoff + header.e_shstrndx * sizeof(Elf64_Shdr)) };

    for (Elf64_Half i{ 0 }; i < header.e_shnum; ++i)
    {
      auto sectionHeader{ this->readRaw<Elf64_Shdr>(header.e_shoff +
                                                    i * sizeof(Elf64_Shdr)) };

      std::string sectionName{ this->readStringRaw(stringTableHeader.sh_offset +
                                                   sectionHeader.sh_name) };

      if (sectionName == name)
      {
        return sectionHeader;
      }
    }

    return {};
  }

  std::optional<
      std::pair<Elf64DataSource::size_type, Elf64DataSource::size_type>>
  Elf64DataSource::findRelocationTable() const noexcept
  {
    if (auto sectionHeader{ this->findSection(".rela.dyn") })
    {
      return std::pair{ sectionHeader->sh_addr,
                        sectionHeader->sh_size / sectionHeader->sh_entsize };
    }

    return {};
  }

  std::optional<
      std::map<Elf64DataSource::size_type, Elf64DataSource::size_type>>
  Elf64DataSource::getRelocations() const noexcept
  {
    if (auto relocations{ findRelocationTable() })
    {
      std::map<size_type, size_type> result{};

      for (size_type i{ 0 }; i < relocations->second; ++i)
      {
        auto relocation{ this->readRaw<Elf64_Rela>(relocations->first +
                                                   i * sizeof(Elf64_Rela)) };

        result.emplace(relocation.r_offset, relocation.r_addend);
      }

      return result;
    }
    else
    {
      return {};
    }
  }

  std::string Elf64DataSource::readCString(size_type index) const
  {
    auto indexString{ this->read<std::uint64_t>(index) };

    return this->readString(indexString);
  }

  Elf64DataSource::size_type Elf64DataSource::readRelativePointer(
      size_type instructionIndex,
      size_type pointerOffset,
      size_type instructionLength) const
  {
    auto relativePointer{ this->read<std::uint32_t>(instructionIndex + pointerOffset) };
    auto nextInstructionIndex{ instructionIndex + instructionLength };
    
    return (nextInstructionIndex + relativePointer);
  }

  Elf64DataSource::Elf64DataSource(const std::filesystem::path& path)
      : FileSource{ path }
  {
    if (this->isOpen())
    {
      if (this->size() < 4 || (this->readStringRaw(1, 3) != "ELF"))
      {
        this->m_isOpen = false;
        return;
      }

      if (auto relocations{ this->getRelocations() })
      {
        this->m_relocations = relocations.value();
      }

      if (auto dataSection{ this->findSection(".data") })
      {
        this->m_mappedData.offset = dataSection->sh_offset;
        this->m_mappedData.virtualAddressBegin = dataSection->sh_addr;
        this->m_mappedData.virtualAddressEnd = (dataSection->sh_addr + dataSection->sh_size);
      }
    }
  }
}