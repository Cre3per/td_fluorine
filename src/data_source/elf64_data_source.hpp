#if !defined(HPPtd_fluorine_elf64_data_source)
#define HPPtd_fluorine_elf64_data_source

#include "file_source.hpp"

#include <elf.h>

#include <map>
#include <optional>
#include <stdexcept>
#include <string_view>
#include <utility>

namespace td_fluorine
{
  /**
   * Reads an elf64 file and applies relocations.
   */
  class Elf64DataSource : public FileSource
  {
  private:
    /**
     * Mapping from a physical segment to where it's loaded into memory.
     */
    struct SegmentMapping
    {
      /**
       * File offset.
       */
      size_type offset{};

      /**
       * Memory offset.
       */
      size_type virtualAddressBegin{};

      /**
       * One past the end memory offset.
       */
      size_type virtualAddressEnd{};
    };

  private:
    /**
     * Maps old indexes to reloacted indexes (RELA).
     */
    std::map<size_type, size_type> m_relocations{};

    /**
     * Mapped .data section (Mapped in segment 01 (LOAD)).
     */
    SegmentMapping m_mappedData{};

  protected:
    /**
     * @override
     */
    [[nodiscard]] size_type redirect(size_type offset) const override;

  private:
    /**
     * Searches for the first section with the given type.
     * @param name Name of the section to search for.
     * @return The header of the found section.
     */
    [[nodiscard]] std::optional<Elf64_Shdr>
    findSection(std::string_view name) const noexcept;

    /**
     * Searches for the first relocation table.
     * @return Offset and number of elements of the relocation table.
     */
    [[nodiscard]] std::optional<std::pair<size_type, size_type>>
    findRelocationTable() const noexcept;

    /**
     * @return Mapping from old to new indexes
     */
    [[nodiscard]] std::optional<std::map<size_type, size_type>>
    getRelocations() const noexcept;

  public:
    /**
     * Reads a 64-bit pointer from index, then a zero-terminated string from the
     * pointer.
     */
    [[nodiscard]] std::string readCString(size_type index) const;

    /**
     * Reads a relative pointer from an instruction.
     * @param instructionIndex Index of the first byte of the instruction.
     * @param pointerOffset Offset from the first byte of the instruction to the
     *                      relative pointer.
     * @param instructionLength Length of the instruction.
     * @return The absolute address.
     */
    [[nodiscard]] size_type readRelativePointer(
        size_type instructionIndex,
        size_type pointerOffset,
        size_type instructionLength) const;

    /**
     * Like {@link binary_data_source::findAllSignatures}, but limits to a
     * section.
     * Throws if the given section cannot be found.
     * @param sectionName Name of the section to search in.
     */
    [[nodiscard]] std::vector<size_type> findAllSignatures(
        std::span<const std::optional<byte_type>> signature,
        std::string_view sectionName) const
    {
      if (auto section{ this->findSection(sectionName) })
      {
        // We can't use sh_address. It'd work for begin, but end is 1 out of
        // the region that would be redirected.
        return BinaryDataSource::findAllSignatures(signature, section->sh_offset, section->sh_offset + section->sh_size);
      }
      else
      {
        throw std::domain_error{ std::string{ sectionName } + " not found" };
      }
    }

  public:
    /**
     * @override
     * Loads relocations.
     * Fails to open the file if it is not an ELF file.
     */
    Elf64DataSource(const std::filesystem::path& path);
  };
}

#endif