#if !defined(HPPtd_fluorine_binary_data_source)
#define HPPtd_fluorine_binary_data_source

#include <array>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <execution>
#include <iterator>
#include <optional>
#include <span>
#include <sstream>
#include <stdexcept>
#include <vector>

namespace td_fluorine
{
  /**
   * A read-only binary data source with random access.
   */
  class BinaryDataSource
  {
  public:
    using byte_type = std::uint8_t;
    using size_type = std::size_t;

  protected:
    /**
     * Redirects an offset to another offset.
     * Useful for virtual addresses or relocations.
     * @param The offset to be redirected.
     * @return The redirected offset.
     */
    [[nodiscard]] virtual size_type redirect(size_type offset) const
    {
      return offset;
    }

  public:
    /**
     * @return Size of the data in bytes.
     */
    [[nodiscard]] virtual size_type size() const noexcept = 0;

    /**
     * @return True if the given index is valid after redirection.
     */
    [[nodiscard]] bool isValidIndex(size_type index) const noexcept
    {
      return (this->redirect(index) < this->size());
    }

    /**
     * Throws {@link std::out_of_range} if @index is >= {@link size}.
     * Implementations of this function must send @index through {@link
     * redirect}.
     * @param index Index of the byte to read.
     * @return The byte at the given index.
     */
    [[nodiscard]] virtual byte_type at(size_type index) const = 0;

    /**
     * Reads memory starting at the given index.
     * No range checking is performed.
     * T must be default-constructible.
     * @param index Index to start reading from. Will not be redirected.
     * @return The read value.
     */
    template <class T>
    [[nodiscard]] T readRaw(size_type index) const noexcept
    {
      T value;
      std::memcpy(&value, this->begin() + index, sizeof(T));
      return value;
    }

    /**
     * Same as {@link readRaw}, but redirects the index.
     */
    template <class T>
    [[nodiscard]] T read(size_type index) const noexcept
    {
      return this->readRaw<T>(this->redirect(index));
    }

    /**
     * Reads a zero-terminated string starting at the given index.
     * Range checking is performed.
     * @param index Start index. Will not be redirected.
     * @return The string
     */
    [[nodiscard]] std::string readStringRaw(size_type index) const
    {
      if (index >= this->size())
      {
        std::stringstream ss{};
        ss << std::hex << index << " >= " << this->size();
        throw std::out_of_range{ ss.str() };
      }

      std::string result{};

      for (size_type i{ index }; i < this->size(); ++i)
      {
        if (auto ch{ this->readRaw<char>(i) })
        {
          result += ch;
        }
        else
        {
          break;
        }
      }

      return result;
    }

    /**
     * Reads a string starting at the given index.
     * Range checking is performed.
     * @param index Start index. Will not be redirected.
     * @param length Length of the string in bytes.
     * @return The string
     */
    [[nodiscard]] std::string readStringRaw(size_type index, size_type length) const
    {
      if ((index + length) >= (this->size() + 1))
      {
        std::stringstream ss{};
        ss << std::hex << (index + length) << " >= " << (this->size() + 1);
        throw std::out_of_range{ ss.str() };
      }

      std::string result(length, '\0');
      std::memcpy(data(result), this->begin() + index, length);
      return result;
    }

    /**
     * Same as {@link readStringRaw}, but redirects @index.
     */
    [[nodiscard]] std::string readString(size_type index) const
    {
      return this->readStringRaw(this->redirect(index));
    }

    /**
     * Does no perform range checking.
     * @param index Index of the byte to read.
     * @return The byte at the given index.
     */
    [[nodiscard]] virtual byte_type
    operator[](size_type index) const noexcept = 0;

    /**
     * Searches for a signature of bytes.
     * @param signature The byte signature to search for
     * @param begin Index of the first byte to be included in the seach.
     * @param end Index of the first byte after @begin to be excluded from the
     *            search. If this is 0, searches until the end of data.
     * @return The index of the first byte at which the signature matched.
     */
    [[nodiscard]] std::optional<size_type> findSignature(
        std::span<const std::optional<byte_type>> signature,
        size_type begin = 0,
        size_type end = 0) const
    {
      if (end == 0)
      {
        end = this->size();
      }

      auto itBegin{ this->begin() + this->redirect(begin) };
      auto itEnd{ this->begin() + this->redirect(end) };

      // {@link https://gcc.gnu.org/bugzilla/show_bug.cgi?id=94696}
      auto found{ std::search(std::execution::seq, itBegin, itEnd,
                              cbegin(signature), cend(signature),
                              [](auto lhs, const auto& rhs) {
                                return (!rhs.has_value() || (lhs == rhs.value()));
                              }) };

      if (found == itEnd)
      {
        return {};
      }
      else
      {
        return std::distance(this->begin(), found);
      }
    }

    /**
     * See {@link findSignature}
     */
    template <std::size_t Length>
    [[nodiscard]] std::optional<size_type> findSignature(
        const std::array<std::optional<byte_type>, Length>& signature,
        size_type begin = 0,
        size_type end = 0) const
    {
      std::vector v(cbegin(signature), cend(signature));
      return this->findSignature(v, begin, end);
    }

    /**
     * Like {@link findSignature}, but finds all matching indexes of a
     * signature.
     * @param signature The signature to search for.
     * @param begin Index of the first byte to be included in the seach.
     * @param end Index of the first byte after @begin to be excluded from the
                  search. If this is 0, searches until the end of data.
     * @return All matches of the given signature.
     */
    template <std::size_t Length>
    [[nodiscard]] std::vector<size_type> findAllSignatures(
        std::span<const std::optional<byte_type>, Length> signature,
        size_type begin = 0,
        size_type end = 0) const
    {
      if (end == 0)
      {
        end = this->size();
      }

      std::vector<size_type> result{};

      auto itBegin{ this->begin() + this->redirect(begin) };
      auto itEnd{ this->begin() + this->redirect(end) };

      while (true)
      {
        // {@link https://gcc.gnu.org/bugzilla/show_bug.cgi?id=94696}
        auto found{ std::search(std::execution::seq, itBegin, itEnd,
                                cbegin(signature), cend(signature),
                                [](auto lhs, const auto& rhs) {
                                  return (!rhs.has_value() || (lhs == rhs.value()));
                                }) };

        if (found == itEnd)
        {
          break;
        }
        else
        {
          result.emplace_back(std::distance(this->begin(), found));
          itBegin = (found + std::size(signature));
        }
      }

      return result;
    }

    /**
     * @return Begin.
     */
    [[nodiscard]] virtual const byte_type* begin() const noexcept = 0;

    /**
     * @return End.
     */
    [[nodiscard]] virtual const byte_type* end() const noexcept = 0;

  public:
    virtual ~BinaryDataSource() = default;
  };
}

#endif