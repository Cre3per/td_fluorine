#include "command_line.hpp"

#include "configuration.hpp"

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include <exception>
#include <iostream>

namespace td_fluorine
{
  std::optional<ProgramOptions> parseCommandLine(int argc, char* argv[])
  {
    ProgramOptions programOptions{};

    boost::program_options::options_description description{ data(td_fluorine::configuration::k_projectName) };

    {
      boost::program_options::options_description block{ "Required arguments" };
      auto options{ block.add_options() };

      options("input,i", boost::program_options::value(&programOptions.inputPath)->required(), "Path to the client library or a json file containing the intermediate representation.");
      options("output,o", boost::program_options::value(&programOptions.outputPath)->default_value({}), "Write the output to this file. By default, output is written to stdout.");
      options("converter,c,format,f", boost::program_options::value(&programOptions.converterName)->default_value("ir"), "Converter to be used.");

      description.add(block);
    }

    {
      boost::program_options::options_description block{ "Optional arguments" };
      auto options{ block.add_options() };

      options("help", "Display this message");

      description.add(block);
    }

    boost::program_options::variables_map arguments{};

    boost::program_options::command_line_parser parser{ argc, argv };
    parser.options(description);
    parser.allow_unregistered();
    auto parsed{ parser.run() };

    try
    {
      boost::program_options::store(parsed, arguments);
    }
    catch (const std::exception& ex)
    {
      std::cerr << ex.what() << '\n';
      return {};
    }

    if (arguments.contains("help"))
    {
      std::cout << description << '\n';
      return {};
    }

    try
    {
      arguments.notify();
    }
    catch (const std::exception& ex)
    {
      std::cerr << ex.what() << '\n';
      return {};
    }

    auto unrecognized{ boost::program_options::collect_unrecognized(parsed.options, boost::program_options::collect_unrecognized_mode::include_positional) };


    auto lastOption{ end(programOptions.unrecognized) };

    for (const auto& argument : unrecognized)
    {
      if (argument.starts_with("--"))
      {
        lastOption = programOptions.unrecognized.emplace(argument, std::optional<std::string>{}).first;
      }
      else
      {
        if (programOptions.unrecognized.empty())
        {
          std::cerr << "unrecognized command line argument: " << argument << std::endl;
          return {};
        }
        else
        {
          lastOption->second = argument;
        }
      }
    }

    return programOptions;
  }
}