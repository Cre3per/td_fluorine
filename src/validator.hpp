#if !defined(HPPtd_fluorine_validator)
#define HPPtd_fluorine_validator

#include "client/client_struct_holder.hpp"

#include <span>

namespace td_fluorine
{
  class Validator
  {
  private:
    /**
     * @param identifier The identifier to validate.
     * @return True if the given identifier is a valid identifier in C++.
     */
    static bool isValidIdentifier(std::string_view identifier) noexcept;

    /**
     * Searches for overlapping variables.
     * @param clientStructs The client structs.
     * @return True if no overlapping variables are found.
     */
    static bool checkOverlap(const ClientStructHolder& clientStructs) noexcept;

    /**
     * Searches for structs and variables with invalid names.
     * Found invalid identifiers are printed.
     * @param clientStructs The client structs.
     * @return True if no bad identifiers are found.
     */
    static bool checkIdentifiers(const ClientStructHolder& clientStructs) noexcept;

    /**
     * Searches for references by name to structs that don't exist.
     * @param clientStruct The client structs.
     * @return True if all dependencies exist.
     */
    static bool checkDependencies(const ClientStructHolder& clientStructs) noexcept;

  public:
    /**
     * Validates client structs, ie. makes sure than no oddities occurred while
     * dumping.
     * Issues are printed.
     * @param clientStructs The client structs.
     * @return True if the client structs are in good condition.
     */
    static bool validateClientStructs(const ClientStructHolder& clientStructs) noexcept;
  };
}

#endif