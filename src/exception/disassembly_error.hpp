#if !defined(HPPtd_fluorine_disassembly_error)
#define HPPtd_fluorine_disassembly_error

#include <cstddef>
#include <cstdint>
#include <stdexcept>

namespace td_fluorine
{
  class DisassemblyError : public std::runtime_error
  {
  private:
    /**
   * Index at which the error occurred.
   */
    std::size_t m_index{};

    /**
   * First byte of the instruction that we attempted to decode.
   */
    std::uint8_t m_instruction{};

  public:
    /**
   * @return Index at which the error occurred.
   */
    [[nodiscard]] std::size_t index() const noexcept;

    /**
   * @return First byte of the instruction that we attempted to decode.
   */
    [[nodiscard]] std::uint8_t instruction() const noexcept;

  public:
    /**
   * @param index Index at which the error occurred.
   * @param instruction First byte of the instruction that we attempted to
   *                    decode.
   */
    DisassemblyError(std::size_t index, std::uint8_t instruction);
  };
}

#endif