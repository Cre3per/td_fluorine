#include "parse_error.hpp"

#include <sstream>

namespace td_fluorine
{
  ParseError::ParseError(std::size_t index, std::string_view found, std::string_view expected)
      : std::runtime_error{
          [=] {
            std::stringstream ss{};

            ss << "at index " << index << ": found " << found;

            if (!expected.empty())
            {
              ss << ", expected " << expected;
            }

            return ss.str();
          }()
        }
  {
  }

  ParseError::ParseError(std::size_t index, char found, std::string_view expected)
      : ParseError{ index, { &found, 1 }, expected }
  {
  }
}