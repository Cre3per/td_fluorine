#if !defined(HPPtd_fluorine_parse_error)
#define HPPtd_fluorine_parse_error

#include <cstddef>
#include <stdexcept>
#include <string_view>

namespace td_fluorine
{
  /**
   * Indicates that an attempt to merge a variable from one class into into
   * another class has failed.
   */
  class ParseError : public std::runtime_error
  {
  public:
    /**
     * @param index Index at which the error ocurred.
     * @param found Found value.
     * @param expected Expected value.
     */
    ParseError(std::size_t index, std::string_view found, std::string_view expected = {});
    ParseError(std::size_t index, char found, std::string_view expected = {});
  };
}

#endif