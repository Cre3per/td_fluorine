#if !defined(HPPtd_fluorine_bad_merge)
#define HPPtd_fluorine_bad_merge

#include "client/client_variable.hpp"

#include <sstream>
#include <stdexcept>

namespace td_fluorine
{
  /**
   * Indicates that an attempt to merge two variables 
   */
  class BadVariableMerge : public std::runtime_error
  {
  public:
    /**
     * @param source Source variable.
     * @param destination Destination variable.
     * @param propertyName Name of the property that couldn't be merged.
     * @param sourceValue Value of the failed property in the source variable.
     * @param destinationValue Value of the failed property in the destination
     *                         variable.
     */
    template <class T>
    BadVariableMerge(const ClientVariable& source, const ClientVariable& destination,
                     std::string_view propertyName,
                     const T& sourceValue, const T& destinationValue)
        : std::runtime_error{
            [&] {
              std::stringstream ss{};

              ss << std::hex << std::showbase;
              ss << "couldn't merge " << source.name() << " at " << source.offset()
                 << " into " << destination.name() << " at " << destination.offset()
                 << ", because they differ in property " << propertyName
                 << ". source: " << sourceValue << ", destination: " << destinationValue;

              return ss.str();
            }()
          }
    {
    }
  };
}

#endif