#include "bad_merge.hpp"

#include "client/client_struct.hpp"
#include "client/client_variable.hpp"

#include <sstream>

namespace td_fluorine
{
  BadMerge::BadMerge(const ClientStruct& toStruct, const ClientVariable& toVariable,
                     const ClientStruct& fromStruct, const ClientVariable& fromVariable)
      : std::runtime_error{ [&] {
          std::stringstream ss{};

          ss << std::hex;

          ss << "cannot merge "
             << fromStruct.name() << "::" << fromVariable.name() << " with type " << fromVariable.type() << " at offset " << std::hex << std::showbase << fromVariable.offset()
             << " into "
             << toStruct.name() << "::" << toVariable.name() << " with type " << toVariable.type() << " at offset " << toVariable.offset();

          return ss.str();
        }() }
  {
  }
}