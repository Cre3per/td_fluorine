#if !defined(HPPtd_fluorine_recvprop_decode_error)
#define HPPtd_fluorine_recvprop_decode_error

#include <stdexcept>
#include <string>
#include <string_view>

namespace td_fluorine
{
  class ClientStruct;
  class ClientVariable;

  /**
   * Indicates that a struct is referenced by another struct by name, but the
   * referenced struct cannot be found.
   */
  class UnknownStructError : public std::runtime_error
  {
  public:
    /**
     * @param structName Name of the struct that wasn't found.
     */
    UnknownStructError(const std::string& structName);

    /**
     * @param structName Name of the struct that wasn't found.
     * @param referencedBy The struct that's referencing this struct. Either as
     *                     a parent or embedded member.
     */
    UnknownStructError(const std::string& structName, const ClientStruct& referencedBy);

    /**
     * @param structName Name of the struct that wasn't found.
     * @param referencedBy The variable that's referencing this struct.
     */
    UnknownStructError(const std::string& structName, const ClientVariable& referencedBy);
  };
}

#endif