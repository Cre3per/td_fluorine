#include "disassembly_error.hpp"

#include <sstream>

namespace td_fluorine
{
  std::size_t DisassemblyError::index() const noexcept
  {
    return this->m_index;
  }

  std::uint8_t DisassemblyError::instruction() const noexcept
  {
    return this->m_instruction;
  }

  DisassemblyError::DisassemblyError(std::size_t index, std::uint8_t instruction)
      : std::runtime_error{ [=] {
          std::stringstream ss{};

          ss << std::hex << std::showbase;
          ss << "could not disassemble instruction " << static_cast<unsigned int>(instruction) << " at index " << index;

          return ss.str();
        }() },
        m_index{ index },
        m_instruction{ instruction }
  {
  }
}