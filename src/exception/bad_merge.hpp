#if !defined(HPPtd_fluorine_bad_merge)
#define HPPtd_fluorine_bad_merge

#include <stdexcept>

namespace td_fluorine
{
  class ClientStruct;
  class ClientVariable;

  /**
   * Indicates that an attempt to merge a variable from one class into into
   * another class has failed.
   */
  class BadMerge : public std::runtime_error
  {
  public:
    /**
     * @param toStruct Destination struct.
     * @param toVariable Destination variable.
     * @param fromStruct Source struct.
     * @param fromVariable Source variable.
     */
    BadMerge(const ClientStruct& toStruct, const ClientVariable& toVariable,
             const ClientStruct& fromStruct, const ClientVariable& fromVariable);
  };
}

#endif