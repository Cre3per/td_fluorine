#if !defined(HPPtd_fluorine_recvprop_decode_error)
#define HPPtd_fluorine_recvprop_decode_error

#include "SourceSDK/public/dt_common.hpp"

#include <stdexcept>
#include <string_view>

namespace td_fluorine
{
  /**
   * Indicates a missing register when attempting to decode a RecvProp call.
   */
  class RecvPropDecodeError : public std::runtime_error
  {
  public:
    /**
     * @param registerName Name of the register that's missing.
     * @param type Type of the RecvProp
     */
    RecvPropDecodeError(std::string_view registerName, source_sdk::SendPropType type);
  };
}

#endif