#include "recvprop_decode_error.hpp"

#include <sstream>

namespace td_fluorine
{
  RecvPropDecodeError::RecvPropDecodeError(std::string_view registerName, source_sdk::SendPropType type)
      : std::runtime_error{
          [=] {
            std::stringstream ss{};

            ss << "missing register " << registerName << " while trying to decode call to RecvProp function with type " << type;

            return ss.str();
          }()
        }
  {
  }
}