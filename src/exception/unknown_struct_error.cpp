#include "unknown_struct_error.hpp"

#include "client/client_struct.hpp"
#include "client/client_variable.hpp"

#include <string>

namespace td_fluorine
{
  UnknownStructError::UnknownStructError(const std::string& structName)
      : std::runtime_error{ "Struct " + structName + " doesn't exist" }
  {
  }

  UnknownStructError::UnknownStructError(const std::string& structName, const ClientStruct& referencedBy)
      : std::runtime_error{ structName + " is referenced by struct " + std::string{ referencedBy.name() } + ", but " + std::string{ structName } + " doesn't exist" }
  {
  }

  UnknownStructError::UnknownStructError(const std::string& structName, const ClientVariable& referencedBy)
      : std::runtime_error{ structName + " is referenced by variable " + std::string{ referencedBy.name() } + ", but " + std::string{ structName } + " doesn't exist" }
  {
  }
}