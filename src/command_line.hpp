#if !defined(HPPtd_fluorine_command_line)
#define HPPtd_fluorine_command_line

#include <map>
#include <optional>
#include <string>

namespace td_fluorine
{
  struct ProgramOptions
  {
    /**
     * Path to the client library.
     */
    std::string inputPath{};

    /**
     * Path to the file to write to. By default, output is written to stdout.
     */
    std::string outputPath{};

    /**
     * Short name of the converter to be used. Defaults to "ir".
     */
    std::string converterName{};

    /**
     * Unrecognized command line options. Forwarded to converters.
     */
    std::map<std::string, std::optional<std::string>> unrecognized{};
  };

  [[nodiscard]] std::optional<ProgramOptions> parseCommandLine(int argc, char* argv[]);
}

#endif