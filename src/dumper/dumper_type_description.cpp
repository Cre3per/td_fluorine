#include "dumper_type_description.hpp"

#include "client/client_variable.hpp"
#include "exception/disassembly_error.hpp"

#include <SourceSDK/public/datamap.hpp>

#include <array>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <optional>
#include <set>

namespace td_fluorine
{
  ClientVariable DumperTypeDescription::dumpTypeDescription(const Elf64DataSource& data, std::size_t index) const
  {
    auto element{ data.read<source_sdk::typedescription_t>(index) };

    auto fieldName{ data.readCString(index + offsetof(source_sdk::typedescription_t, fieldName)) };

    ClientVariable variable{ fieldName, element.fieldType, static_cast<std::size_t>(element.fieldOffset) };

    if (element.fieldSize > 1)
    {
      variable.elements(element.fieldSize);
    }

    if (element.fieldType == source_sdk::fieldtype_t::FIELD_EMBEDDED)
    {
      auto indexPTD{ index + offsetof(source_sdk::typedescription_t, td) };
      auto indexTD{ data.read<std::uint64_t>(indexPTD) };

      auto dataClassName{ data.readCString(indexTD + offsetof(source_sdk::datamap_t, dataClassName)) };

      variable.embeddedType(dataClassName);
    }

    return variable;
  }

  std::vector<DumperTypeDescription::TypeDescriptionInformation> DumperTypeDescription::collectAndDissect(
      const Elf64DataSource& data,
      std::span<const std::optional<std::uint8_t>> signature,
      std::function<TypeDescriptionInformation(std::size_t)> dissector) const
  {
    auto matches{ data.findAllSignatures(signature, ".text") };

    std::vector<TypeDescriptionInformation> result(matches.size());

    std::ranges::transform(matches, begin(result), [&](auto index) {
      return dissector(index);
    });

    return result;
  }

  std::vector<DumperTypeDescription::TypeDescriptionInformation>
  DumperTypeDescription::collectPredMapInit(
      const Elf64DataSource& data) const
  {
    // clang-format off
    constexpr std::array<std::optional<std::uint8_t>, 37> signature{
      0x55, 0x48, 0x8d, 0x05, {}, {}, {}, {},
      0x48, 0x89, 0xe5,
      0x5d,
      0x48, 0x89, 0x05, {}, {}, {}, {},
      0xc7, {}, {},  {}, {}, {}, {}, {}, {}, {},
      0x48, 0x8d, 0x05, {}, {}, {}, {},
      0xc3
    };
    // clang-format on

    return this->collectAndDissect(data, signature, [&](auto index) {
      return TypeDescriptionInformation{
        .typeDescriptions{ data.readRelativePointer(index + 0x01, 0x03, 0x07) },
        .dataMap{ data.readRelativePointer(index + 0x0c, 0x03, 0x07) },
        .dataNumFields{ static_cast<std::size_t>(data.read<decltype(source_sdk::datamap_t::dataNumFields)>(index + 0x13 + 0x06)) }
      };
    });
  }

  // C_ClientRagdoll
  std::vector<DumperTypeDescription::TypeDescriptionInformation>
  DumperTypeDescription::collectDataMapInit1(
      const Elf64DataSource& data) const
  {
    // clang-format off
    constexpr std::array<std::optional<std::uint8_t>, 32> signature{
      0x80, {}, {}, {}, {}, {}, {},
      0x55,
      0x48, 0x89, 0xe5,
      0x74, {},
      0xc7, {}, {}, {}, {}, {}, {}, {}, {}, {},
      0x48, {}, {}, {}, {}, {}, {},
      0x5d,
      0x48
    };
    // clang-format on

    return this->collectAndDissect(data, signature, [&data](std::size_t index) {
      return TypeDescriptionInformation{
        .typeDescriptions{ data.readRelativePointer(index + 0x26, 0x03, 0x07) },
        .dataMap{ data.readRelativePointer(index + 0x2d, 0x03, 0x07) },
        .dataNumFields{ static_cast<std::size_t>(data.read<decltype(source_sdk::datamap_t::dataNumFields)>(index + 0x0d + 0x06)) }
      };
    });
  }

  // CBaseAchievement
  std::vector<DumperTypeDescription::TypeDescriptionInformation>
  DumperTypeDescription::collectDataMapInit2(
      const Elf64DataSource& data) const
  {
    // clang-format off
    constexpr std::array<std::optional<std::uint8_t>, 22> signature{
      0x80, {}, {}, {}, {}, {}, {},
      0x55,
      0x48, 0x89, 0xe5,
      0x74, {},
      0x48, {}, {}, {}, {}, {}, {},
      0x5d,
      0x48
    };
    // clang-format on

    return this->collectAndDissect(data, signature, [&data](std::size_t index) {
      return TypeDescriptionInformation{
        .typeDescriptions{ data.readRelativePointer(index + 0x0d, 0x03, 0x07) },
        .dataMap{ data.readRelativePointer(index + 0x20, 0x03, 0x07) },
        .dataNumFields{ static_cast<std::size_t>(data.read<decltype(source_sdk::datamap_t::dataNumFields)>(index + 0x27 + 0x06)) }
      };
    });
  }

  // CFailableAchievement
  std::vector<DumperTypeDescription::TypeDescriptionInformation>
  DumperTypeDescription::collectDataMapInit3(
      const Elf64DataSource& data) const
  {
    // clang-format off
    constexpr std::array<std::optional<std::uint8_t>, 22> signature{
      0x80, {}, {}, {}, {}, {}, {},
      0x55,
      0x48, 0x89, 0xe5,
      0x74, {},
      0x48, {}, {}, {}, {}, {}, {},
      0x5d,
      0xc7
    };
    // clang-format on

    return this->collectAndDissect(data, signature, [&data](std::size_t index) {
      return TypeDescriptionInformation{
        .typeDescriptions{ data.readRelativePointer(index + 0x26, 0x03, 0x07) },
        .dataMap{ data.readRelativePointer(index + 0x2d, 0x03, 0x07) },
        .dataNumFields{ static_cast<std::size_t>(data.read<decltype(source_sdk::datamap_t::dataNumFields)>(index + 0x15 + 0x06)) }
      };
    });
  }

  // CCopyRecipientFilter
  std::vector<DumperTypeDescription::TypeDescriptionInformation>
  DumperTypeDescription::collectDataMapInit4(
      const Elf64DataSource& data) const
  {
    // clang-format off
    constexpr std::array<std::optional<std::uint8_t>, 21> signature{
      0x80, {}, {}, {}, {}, {}, {},
      0x55,
      0x48, 0x89, 0xe5,
      0x74, {},
      0x80, {}, {}, {}, {}, {}, {},
      0x48
    };
    // clang-format on

    return this->collectAndDissect(data, signature, [&data](std::size_t index) {
      return TypeDescriptionInformation{
        .typeDescriptions{ data.readRelativePointer(index + 0x21, 0x03, 0x07) },
        .dataMap{ data.readRelativePointer(index + 0x33, 0x03, 0x07) },
        .dataNumFields{ static_cast<std::size_t>(data.read<decltype(source_sdk::datamap_t::dataNumFields)>(index + 0x29 + 0x06)) }
      };
    });
  }

  // CRagdoll
  std::vector<DumperTypeDescription::TypeDescriptionInformation>
  DumperTypeDescription::collectDataMapInit5(
      const Elf64DataSource& data) const
  {
    // clang-format off
    constexpr std::array<std::optional<std::uint8_t>, 21> signature{
      0x55,
      0x48, 0x89, 0xe5,
      0x53,
      0x48, 0x83, 0xec, 0x08,
      0x80, {}, {}, {}, {}, {}, {},
      0x0f
    };
    // clang-format on

    return this->collectAndDissect(data, signature, [&data](std::size_t index) {
      return TypeDescriptionInformation{
        .typeDescriptions{ data.readRelativePointer(index + 0x2a, 0x03, 0x07) },
        .dataMap{ data.readRelativePointer(index + 0x3b, 0x03, 0x07) },
        .dataNumFields{ static_cast<std::size_t>(data.read<decltype(source_sdk::datamap_t::dataNumFields)>(index + 0x31 + 0x06)) }
      };
    });
  }

  ClientStruct DumperTypeDescription::dumpClass(const Elf64DataSource& data, const TypeDescriptionInformation& information) const
  {
    static const std::map<std::string_view, std::set<std::string_view>> ignoredVariables{
      { "C_BaseEntity", { "m_angRotation", "m_vecOrigin" } } // RECVINFO_NAME
    };

    auto dataClassName{ data.readCString(information.dataMap + offsetof(source_sdk::datamap_t, dataClassName)) };
    ClientStruct result{ dataClassName };

    if (auto indexBaseMap{ data.read<std::uint64_t>(information.dataMap + offsetof(source_sdk::datamap_t, baseMap)) })
    {
      auto parentClassName{ data.readCString(indexBaseMap + offsetof(source_sdk::datamap_t, dataClassName)) };
      result.parentName(parentClassName);
    }

    if (!data.isValidIndex(information.typeDescriptions))
    {
      // This happens to classes that are predictable, but have no predictable
      // fields.
      return result;
    }

    ClientStruct clientStruct{ dataClassName };

    const auto* ignoredMembers{
      [&]() -> const std::set<std::string_view>* {
        if (auto found{ ignoredVariables.find(dataClassName) };
            found != cend(ignoredVariables))
        {
          return &found->second;
        }

        return nullptr;
      }()
    };

    for (std::size_t i{ 0 }; i < information.dataNumFields; ++i)
    {
      auto indexElement{ information.typeDescriptions + i * sizeof(source_sdk::typedescription_t) };

      auto clientVariable{ this->dumpTypeDescription(data, indexElement) };

      if ((ignoredMembers == nullptr) || !ignoredMembers->contains(clientVariable.name()))
      {
        clientStruct.addVariable(std::move(clientVariable));
      }
    }

    return clientStruct;
  }

  ClientStructHolder DumperTypeDescription::dump(const Elf64DataSource& data)
  {
    std::vector<TypeDescriptionInformation> typeDescriptions{};
    for (auto fn : {
             &DumperTypeDescription::collectPredMapInit,
             &DumperTypeDescription::collectDataMapInit1,
             &DumperTypeDescription::collectDataMapInit2,
             &DumperTypeDescription::collectDataMapInit3,
             &DumperTypeDescription::collectDataMapInit4,
             &DumperTypeDescription::collectDataMapInit5 })
    {
      auto found{ (this->*fn)(data) };
      typeDescriptions.insert(typeDescriptions.end(), std::make_move_iterator(found.begin()), std::make_move_iterator(found.end()));
    }

    ClientStructHolder result(typeDescriptions.size());

    std::ranges::transform(typeDescriptions, std::inserter(result, result.begin()), [&, this](auto index) {
      return this->dumpClass(data, index);
    });

    return result;
  }
}
