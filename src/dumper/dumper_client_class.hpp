#if !defined(HPPtd_fluorine_dumper_clientclass)
#define HPPtd_fluorine_dumper_clientclass

#include "dumper.hpp"
#include "recvprop_disassembler.hpp"

#include "client/client_struct.hpp"
#include "client/client_struct_holder.hpp"
#include "client/client_variable.hpp"
#include "data_source/elf64_data_source.hpp"

#include <SourceSDK/public/dt_common.hpp>

#include <cstdint>
#include <deque>
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <variant>
#include <vector>

namespace td_fluorine
{
  /**
   * Dumps client structs/variables exposed through RecvProps.
   */
  class DumperClientClass : public Dumper
  {
  private:
    /**
     * Maps indexes into the current data to RecvProp functions.
     * This map gets populated as known variables get discovered.
     */
    std::map<std::size_t, source_sdk::SendPropType> m_recvPropFunctions{};

    /**
     * The struct that's currently being dumped.
     * Used for logging. Guaranteed to be set while dumping variables.
     */
    const ClientStruct* m_activeStruct{};

  private:
    /**
     * Searches for the built-up of the first call to a RecvProp function after
     * the RecvTable::Construct call.
     * @param data The data source.
     * @param indexLeaNetTableName Index of the lea instruction that loads
     *                             pNetTableName for RecvTable::Construct.
     * @return Index of the first instruction that prepares the call to a
     *         RecvProp function. Usually a xor.
     */
    [[nodiscard]] std::optional<std::size_t> findFirstRecvPropCall(
        const Elf64DataSource& data,
        std::size_t indexLeaNetTableName) const;

    /**
     * Updates @target with information obtained from @array, such that @target
     * If @target's offset or name is different from that of @array, warnings
     * are printed and false is returned.
     * becomes an array and @array can be discarded.
     * @param target The variable to promote to an array.
     * @param array The array without type information.
     * @return True if the merge was successful.
     */
    bool mergeArray(ClientVariable& target, const ClientVariable& array) const;

    /**
     * Searches for all RecvProp functions and updates
     * {@link m_recvPropFunctions}.
     * Throws if not all functions can be found.
     * @param data The data source.
     */
    void findRecvPropFunctions(const Elf64DataSource& data);

    /**
     * Converts the name of a data table ("DT_*") into a name of a client
     * class ("C_*").
     * @param dataTableName The name of the data table.
     * @return The name of the corresponding client class.
     */
    [[nodiscard]] std::string
    dataTableNameToClientClassName(const std::string& dataTableName) const;

    /**
     * Adds client variables to a client struct, merging array variables.
     * @param clientStruct The client struct to fill with variables.
     * @param clientVariables The variables to fill the client struct with.
     */
    void fillClientStruct(ClientStruct& clientStruct,
                          std::vector<ClientVariable>&& clientVariables) const;

    /**
     * Attemps to decode a call to a RecvProp function. If the type of the
     * function is not yet known decoding fails. Throws if the registers have
     * not been populated enough to create a {@link ClientVariable} or if the
     * called function is unknown.
     * @param data The data source.
     * @param call The RecvProp call.
     * @return Nothing if this call has no meaningful variable, otherwise the
     *         {@link ClientVariable}. If the returned variable is an array, the
     *         type has not been set.
     */
    [[nodiscard]] std::optional<ClientVariable> decodeRecvPropCall(
        const Elf64DataSource& data,
        const RecvPropCall& call) const;

    /**
     * Disassembles the calls to RecvProp functions inside
     * ClientClassInit<T::ignore> and extracts client variables.
     * @param data The data source.
     * @param variableIndex Index of the variable (Index in the variable array,
     *                      not data). Used for logging.
     * @param index The index of the first byte of the preparation to a call to
     *              a RecvProp function. This is usually the index of a xor.
     *              When this call ends and parsing was successful, @index is
     *              the index of the next instruction after the call. Otherwise
     *              it is left unmodified.
     * @return The disassembled {@link RecvPropCall}
     */
    [[nodiscard]] std::optional<ClientVariable> dumpRecvPropCall(
        const Elf64DataSource& data,
        std::size_t variableIndex,
        std::size_t& index) const;

    /**
     * Dumps client variables registered via RecvProp functions.
     * Does not merge arrays.
     * @param data The data source.
     * @param index The index of the first instruction that prepares a RecvProp
     *              call. Usually a xor.
     */
    [[nodiscard]] std::vector<ClientVariable> dumpClientVariables(
        const Elf64DataSource& data,
        std::size_t index) const;

    /**
     * Dumps basic information about a client struct. Specifically, the name.
     * @param data The data source.
     * @param index Index of the lea instruction that loads pNetTableName in preparation for
     * the call to RecvTable::Construct.
     * @return The client struct with a name.
     */
    [[nodiscard]] ClientStruct dumpClientClassHead(const Elf64DataSource& data, std::size_t index) const;

    /**
     * Dumps a client class from a single ClientClassInit<T::ignore> function.
     * @param data The data source.
     * @param index The index of the lea instruction that loads the table
     *              name (This is not the first byte of the function).
     */
    ClientStruct dumpClientClassInit(
        const Elf64DataSource& data,
        std::size_t index);

  public:
    /**
     * @override
     */
    ClientStructHolder dump(const Elf64DataSource& data) override;
  };
}

#endif