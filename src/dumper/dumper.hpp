#if !defined(HPPtd_fluorine_dumper)
#define HPPtd_fluorine_dumper

#include "client/client_struct_holder.hpp"
#include "data_source/elf64_data_source.hpp"

#include <vector>

namespace td_fluorine
{
  class Dumper
  {
  public:
    /**
     * Dumps client structs.
     * @param data The binary data source.
     * @return The dumped structs.
     */
    virtual ClientStructHolder dump(const Elf64DataSource& data) = 0;

  public:
    virtual ~Dumper() = default;
  };
}

#endif