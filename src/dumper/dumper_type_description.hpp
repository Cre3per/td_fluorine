#if !defined(HPPtd_fluorine_dumper_prediction)
#define HPPtd_fluorine_dumper_prediction

#include "dumper.hpp"

#include "client/client_struct.hpp"
#include "client/client_struct_holder.hpp"
#include "data_source/elf64_data_source.hpp"

#include <cstdint>
#include <optional>
#include <span>

namespace td_fluorine
{
  /**
   * Dumps client structs/variables exposed through prediction tables.
   */
  class DumperTypeDescription : public Dumper
  {
  private:
    /**
     * Information that can be extracted from a PredMapInit<T> function.
     */
    struct TypeDescriptionInformation
    {
      /**
       * Offset of the {@link typedescription_t} array (predDesc, dataDesc).
       */
      std::size_t typeDescriptions{};

      /**
       * Offset of the {@link datamap_t} (m_PredMap, m_DataMap)
       */
      std::size_t dataMap{};

      /**
       * Number of fields in this class.
       */
      std::size_t dataNumFields{};
    };

  private:
    /**
     * Reads {@link source_sdk::datamap_t::dataClassName}.
     * @param data The data source.
     * @param index The index of the first byte of the
     *              {@link source_sdk::datamap_t}.
     * @return {@link source_sdk::datamap_t::dataClassName} 
     */
    [[nodiscard]] std::string getDataMapClassName(const Elf64DataSource& data, std::size_t index) const;

    /**
     * Dumps a client variable from a {@link source_sdk::typedescription_t} that
     * was placed in the .data section.
     * @param data The data source.
     * @param index The index of the first byte of the
     *              {@link source_sdk::typedescription_t} struct.
     * @return The dumped client variable.
     */
    [[nodiscard]] ClientVariable dumpTypeDescription(const Elf64DataSource& data, std::size_t index) const;

    /**
     * Searches for all occurrences of the given signature and passes the found
     * signatures to the given dissector function.
     * @param data The data source.
     * @param signature The signature to search for.
     * @param dissector The dissector function.
     */
    [[nodiscard]] std::vector<TypeDescriptionInformation> collectAndDissect(
        const Elf64DataSource& data,
        std::span<const std::optional<std::uint8_t>> signature,
        std::function<TypeDescriptionInformation(std::size_t)> dissector) const;

    /**
     * Searches for all PredMapInit<T> functions and dissects them.
     * @param data The data source.
     */
    [[nodiscard]] std::vector<TypeDescriptionInformation> collectPredMapInit(const Elf64DataSource& data) const;

    /**
     * Searches for all DataMapInit<T> functions and dissects them.
     * @param data The data source.
     */
    [[nodiscard]] std::vector<TypeDescriptionInformation> collectDataMapInit1(const Elf64DataSource& data) const;
    [[nodiscard]] std::vector<TypeDescriptionInformation> collectDataMapInit2(const Elf64DataSource& data) const;
    [[nodiscard]] std::vector<TypeDescriptionInformation> collectDataMapInit3(const Elf64DataSource& data) const;
    [[nodiscard]] std::vector<TypeDescriptionInformation> collectDataMapInit4(const Elf64DataSource& data) const;
    [[nodiscard]] std::vector<TypeDescriptionInformation> collectDataMapInit5(const Elf64DataSource& data) const;

    /**
     * Searches for all DataMapInit<T> functions and dissects them.
     * (Shared types)
     * @param data The data source.
     */
    [[nodiscard]] std::vector<TypeDescriptionInformation> collectDataMapInitShared(const Elf64DataSource& data) const;

    /**
     * Dumps a client struct based on a PredMapInit<T> function.
     * If a struct cannot be fully dumped, a warning is printed.
     * @param data The data source.
     * @param information The type description offsets.
     * @return The dumped client struct.
     */
    [[nodiscard]] ClientStruct dumpClass(const Elf64DataSource& data, const TypeDescriptionInformation& information) const;

  public:
    /**
     * @override
     */
    ClientStructHolder dump(const Elf64DataSource& data) override;
  };
}

#endif