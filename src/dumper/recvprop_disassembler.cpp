#include "recvprop_disassembler.hpp"

#include "exception/disassembly_error.hpp"

namespace td_fluorine
{
  // I know nothing about assembly encoding, so this parser is probably
  // painful to read for people who how it should be done properly.
  // This parser is incomplete, we're not extracting all the registers we
  // could.
  RecvPropCall RecvPropDisassembler::disassemble(const Elf64DataSource& data, std::size_t& index) const
  {
    RecvPropCall result{};

    // Index of the current instruction.
    std::size_t indexInstruction{ index };

    while (true)
    {
      auto instruction{ data.at(indexInstruction) };

      switch (instruction)
      {
      case 0x31: // xor 32bit register
        switch (auto targetRegister{ data.at(indexInstruction + 0x01) })
        {
        case 0xc0:
          result.r8 = 0;
          break;
        case 0xd2:
          result.rdx = 0;
          break;
        case 0xf6:
          result.rsi = 0;
          break;
        }
        indexInstruction += 0x02;
        break;
      case 0x41: // mov
        switch (auto targetRegister{ data.at(indexInstruction + 0x01) })
        {
        case 0xb8:
          result.r8 = data.read<std::uint32_t>(indexInstruction + 0x02);
          break;
        }
        indexInstruction += 0x06;
        break;
      case 0x45: // xor 64bit register
        switch (auto targetRegister{ data.at(indexInstruction + 0x02) })
        {
        case 0xc0:
          result.r8 = 0;
          break;
        }
        indexInstruction += 0x03;
        break;
      case 0x48: // mov or lea
      {
        switch (auto targetRegister{ data.at(indexInstruction + 0x02) })
        {
        case 0x04: // stack
          indexInstruction += 0x04;
          break;
        case 0x0d:
          result.rcx = (indexInstruction + 0x07 + data.read<std::uint32_t>(indexInstruction + 0x03));
          indexInstruction += 0x07;
          break;
        case 0x35:
          result.rsi = (indexInstruction + 0x07 + data.read<std::uint32_t>(indexInstruction + 0x03));
          indexInstruction += 0x07;
          break;
        case 0x44: // stack
          indexInstruction += 0x05;
          break;
        case 0x45: // stack
          indexInstruction += 0x04;
          break;
        case 0x7d: // rdi
          indexInstruction += 0x04;
          break;
        default:
          indexInstruction += 0x07;
          break;
        }

        break;
      }
      case 0x49: // mov
        indexInstruction += 0x03;
        break;
      case 0x4c: // mov
        switch (auto targetRegister{ data.at(indexInstruction + 0x02) })
        {
        case 0x00: // r8
          indexInstruction += 0x03;
          break;
        case 0x0d: // r9
          indexInstruction += 0x07;
          break;
        case 0x25: // r12
          indexInstruction += 0x07;
          break;
        default:
          result.r8 = data.read<std::uint32_t>(indexInstruction + 0x03);
          indexInstruction += 0x07;
          break;
        }
        break;
      case 0x4d: // mov
        indexInstruction += 0x03;
        break;
      case 0xb9: // mov
        result.rcx = data.read<std::uint32_t>(indexInstruction + 0x01);
        indexInstruction += 0x05;
        break;
      case 0xba: // mov
        result.rdx = data.read<std::uint32_t>(indexInstruction + 0x01);
        indexInstruction += 0x05;
        break;
      case 0xbe: // mov
        result.rsi = data.read<std::uint32_t>(indexInstruction + 0x01);
        indexInstruction += 0x05;
        break;
      case 0xc7: // mov
        indexInstruction += 0x07;
        break;
      case 0xe8: // call
        result.callTarget = static_cast<std::size_t>((static_cast<std::ptrdiff_t>(indexInstruction) + 0x05 + data.read<std::int32_t>(indexInstruction + 0x01)));
        index = (indexInstruction + 0x05);
        return result;
        break;
      default:
        throw DisassemblyError{ indexInstruction, instruction };
        break;
      }
    }
  }
}