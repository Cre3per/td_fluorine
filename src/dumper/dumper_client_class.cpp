#include "dumper_client_class.hpp"
#include "recvprop_disassembler.hpp"

#include "client/client_variable.hpp"
#include "data_source/binary_data_source.hpp"
#include "data_source/elf64_data_source.hpp"
#include "exception/recvprop_decode_error.hpp"

#include <SourceSDK/public/datamap.hpp>
#include <SourceSDK/public/dt_common.hpp>

#include <array>
#include <cassert>
#include <cstdint>
#include <cstddef>
#include <exception>
#include <iostream>
#include <iterator>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <string_view>
#include <variant>

namespace td_fluorine
{
  std::optional<std::size_t> DumperClientClass::findFirstRecvPropCall(
      const Elf64DataSource& data,
      std::size_t indexLeaNetTableName) const
  {
    // clang-format off
    constexpr std::array<std::optional<std::uint8_t>, 3> signatureVariableRegistration{
      0x45, 0x31, 0xc9 // xor r9d, r9d
    };
    // clang-format on

    // Maximum number of bytes that the call to RecvTable::Construct takes up.
    constexpr std::size_t recvTableConstructMaximumLength{ 0x40 };

    if (auto found{ data.findSignature(signatureVariableRegistration,
                                       indexLeaNetTableName,
                                       indexLeaNetTableName + recvTableConstructMaximumLength) })
    {
      return found.value();
    }

    return {};
  }

  bool DumperClientClass::mergeArray(ClientVariable& target, const ClientVariable& array) const
  {
    // Arrays registered via InternalRecvPropArray don't have an offset.
    if ((array.offset() != 0) && (target.offset() != array.offset()))
    {
      std::cerr << std::hex << std::showbase;
      std::cerr << "warn: tried to promote " << this->m_activeStruct->name() << "::" << target.name() << " at " << target.offset()
                << " to an array by using " << this->m_activeStruct->name() << "::" << array.name() << " at " << array.offset()
                << ", but their offsets don't match" << std::endl;

      return false;
    }

    std::string_view manualName{ target.name() };

    if (auto found{ manualName.find_last_of('[') }; found != std::string_view::npos)
    {
      manualName.remove_suffix(manualName.length() - found);
    }

    if (manualName != array.name())
    {
      std::cerr << std::hex << std::showbase;
      std::cerr << "warn: promoting " << this->m_activeStruct->name() << "::" << target.name() << " at " << target.offset()
                << " to an array by using " << this->m_activeStruct->name() << "::" << array.name() << " at " << array.offset()
                << ", even though their names don't match" << std::endl;
    }

    target.name(array.name());
    target.elements(array.elements());

    return true;
  }

  void DumperClientClass::findRecvPropFunctions(const Elf64DataSource& data)
  {
    struct RecvPropSignature
    {
      std::vector<std::optional<std::uint8_t>> signature{};
      source_sdk::SendPropType type{};
    };

    // The signatures are sorted in their order of appearance in the binary.
    // That way we can identify different RecvProp functions with identical
    // signatures and we get away with really short signatures.
    static const std::vector<RecvPropSignature> signatures{
      { { 0x55, 0x4d, 0x89, 0xc1, 0x45, 0x31, 0xc0, 0x48, 0x89, 0xe5, 0x53 }, source_sdk::SendPropType::Int }, // IntWithMinusOneFlag
      { { 0x55, 0x4d, 0x89, 0xc1 }, source_sdk::SendPropType::EHandle },
      { { 0x55, 0x45, 0x31, 0xc9 }, source_sdk::SendPropType::Bool },
      { { 0x55, 0x45, 0x31, 0xc0 }, source_sdk::SendPropType::Float }, // Time
      { { 0x55, 0x48, 0x89, 0xf8, 0x48, 0x89, 0xe5, 0x48, 0xc7, 0x47, 0x18, 0x00, 0x00, 0x00, 0x00, 0x5d }, source_sdk::SendPropType::Float },
      { { 0x55, 0x48, 0x89, 0xf8 }, source_sdk::SendPropType::Vector },
      { { 0x55, 0x48, 0x89, 0xf8 }, source_sdk::SendPropType::VectorXY },
      { { 0x55, 0x4d, 0x85, 0xc9 }, source_sdk::SendPropType::Int },
      { { 0x55, 0x48, 0x89, 0xf8 }, source_sdk::SendPropType::String },
      { { 0x55, 0x48, 0x89, 0xf8 }, source_sdk::SendPropType::DataTable },
      { { 0x55, 0x48, 0x89, 0xf8 }, source_sdk::SendPropType::Array }, // Array3
      { { 0x55, 0x48, 0x89, 0xf8 }, source_sdk::SendPropType::InternalArray }, // InternalRecvPropArray
      { { 0x55, 0x48, 0x89, 0xe5, 0x41, 0x57, 0x4d, 0x89 }, source_sdk::SendPropType::UtlVector }
    };

    std::size_t begin{ 0 };

    for (const auto& signature : signatures)
    {
      if (auto found{ data.findSignature(signature.signature, begin) })
      {
        this->m_recvPropFunctions[found.value()] = signature.type;
        begin = (found.value() + size(signature.signature));
      }
      else
      {
        std::stringstream ss{};
        ss << "signature for RecvProp " << signature.type << " not found";
        throw std::runtime_error{ ss.str() };
      }
    }
  }

  std::string DumperClientClass::dataTableNameToClientClassName(const std::string& dataTableName) const
  {
    if (dataTableName.starts_with("DT_"))
    {
      return ("C_" + dataTableName.substr(3));
    }
    else
    {
      throw std::runtime_error{ "cannot convert \"" + dataTableName + "\" to a client class name." };
    }
  }

  void DumperClientClass::fillClientStruct(
      ClientStruct& clientStruct,
      std::vector<ClientVariable>&& clientVariables) const
  {
    for (std::size_t i{ size(clientVariables) }; i-- > 0;)
    {
      auto& clientVariable{ clientVariables[i] };

      if (clientVariable.elements() == 0)
      {
        clientStruct.addVariable(std::move(clientVariables[i]));
      }
      else
      {
        auto& previousVariable{ clientVariables[i - 1] };

        if (this->mergeArray(previousVariable, clientVariable))
        {
          clientStruct.addVariable(std::move(previousVariable));
          --i;
        }
        else
        {
          std::cerr << "warn: ignoring " << clientVariable.name()
                    << ", because it cannot be merged.";
        }
      }
    }
  }

  std::optional<ClientVariable> DumperClientClass::decodeRecvPropCall(
      const Elf64DataSource& data,
      const RecvPropCall& call) const
  {
    static const std::map<source_sdk::SendPropType, source_sdk::fieldtype_t> typeMap{
      { source_sdk::SendPropType::Int, source_sdk::fieldtype_t::FIELD_INTEGER },
      { source_sdk::SendPropType::Float, source_sdk::fieldtype_t::FIELD_FLOAT },
      { source_sdk::SendPropType::Vector, source_sdk::fieldtype_t::FIELD_VECTOR },
      { source_sdk::SendPropType::VectorXY, source_sdk::fieldtype_t::FIELD_VECTOR2D },
      { source_sdk::SendPropType::String, source_sdk::fieldtype_t::FIELD_STRING },
      { source_sdk::SendPropType::Array, source_sdk::fieldtype_t::FIELD_VOID }, // Type is set later
      { source_sdk::SendPropType::InternalArray, source_sdk::fieldtype_t::FIELD_VOID }, // Type is set later
      { source_sdk::SendPropType::DataTable, source_sdk::fieldtype_t::FIELD_VOID }, // Unused
      { source_sdk::SendPropType::Int64, source_sdk::fieldtype_t::FIELD_INTEGER },
      { source_sdk::SendPropType::EHandle, source_sdk::fieldtype_t::FIELD_EHANDLE },
      { source_sdk::SendPropType::Bool, source_sdk::fieldtype_t::FIELD_BOOLEAN },
      { source_sdk::SendPropType::UtlVector, source_sdk::fieldtype_t::FIELD_UTLVECTOR }
    };

    if (auto found{ this->m_recvPropFunctions.find(call.callTarget) };
        found != cend(this->m_recvPropFunctions))
    {
      source_sdk::SendPropType type{ found->second };

      ClientVariable clientVariable{};
      clientVariable.type(typeMap.at(type));

      switch (type)
      {
      case source_sdk::SendPropType::Bool:
      case source_sdk::SendPropType::Int:
      case source_sdk::SendPropType::Float:
      case source_sdk::SendPropType::Vector:
      case source_sdk::SendPropType::VectorXY:
      case source_sdk::SendPropType::String:
      case source_sdk::SendPropType::Array:
      case source_sdk::SendPropType::Int64:
      case source_sdk::SendPropType::EHandle:
      case source_sdk::SendPropType::UtlVector:
      {
        if (!call.rsi.has_value())
        {
          throw RecvPropDecodeError{ "rsi", type };
        }
        if (!call.rdx.has_value())
        {
          throw RecvPropDecodeError{ "rdx", type };
        }

        if (call.rsi.value() == 0)
        {
          std::cerr << "warn: variable in " << this->m_activeStruct->name()
                    << " at offset " << std::hex << std::showbase << call.rdx.value()
                    << " has no name" << std::endl;

          std::stringstream ss{};
          ss << "MissingNo_" << std::hex << call.rdx.value();
          clientVariable.name(ss.str());
        }
        else
        {
          clientVariable.name(data.readString(call.rsi.value()));
        }

        clientVariable.offset(call.rdx.value());

        if (type == source_sdk::SendPropType::Array)
        {
          if (!call.r8.has_value())
          {
            throw RecvPropDecodeError{ "r8", type };
          }

          // At this point, the type of the elements is unknown (FIELD_VOID).
          // This variable will be merged with the previous one, which knows the
          // type.
          clientVariable.elements(call.r8.value());
        }
        else if (type == source_sdk::SendPropType::Int)
        {
          if (!call.rcx.has_value())
          {
            throw RecvPropDecodeError{ "rcx", type };
          }

          // Demote narrow integers
          auto variableSize{ static_cast<int>(call.rcx.value()) };
          if (variableSize != static_cast<int>(source_sdk::getFieldTypeSize(source_sdk::fieldtype_t::FIELD_INTEGER)))
          {
            if (variableSize == 1)
            {
              clientVariable.type(source_sdk::fieldtype_t::FIELD_CHARACTER);
            }
            else if (variableSize == 2)
            {
              clientVariable.type(source_sdk::fieldtype_t::FIELD_SHORT);
            }
            else if (variableSize == 8)
            {
              clientVariable.type(source_sdk::fieldtype_t::FIELD_INTEGER64);
            }
            else if (variableSize == -1)
            {
              // SIZEOF_IGNORE
            }
            else
            {
              std::cerr << "warn: " << this->m_activeStruct->name() << "::" << clientVariable.name() << " is an integer but has size " << std::dec << variableSize << std::endl;
            }
          }
        }

        return clientVariable;
      }
      case source_sdk::SendPropType::InternalArray:
      {
        // InternalRecvPropArray doesn't receive an offset.
        if (!call.rcx.has_value())
        {
          throw RecvPropDecodeError{ "rcx", type };
        }
        if (!call.rsi.has_value())
        {
          throw RecvPropDecodeError{ "rsi", type };
        }

        clientVariable.name(ClientVariable::sanitizeName(data.readString(call.rcx.value())));
        clientVariable.elements(call.rsi.value());

        return clientVariable;
      }
      case source_sdk::SendPropType::DataTable:
        return {};
        break;
      default:
        assert(false);
        return {};
        break;
      }
    }
    else
    {
      std::stringstream ss{};
      ss << std::hex << std::showbase << call.callTarget << " is not a known RecvProp function";
      throw std::runtime_error{ ss.str() };
    }
  }

  std::optional<ClientVariable> DumperClientClass::dumpRecvPropCall(
      const Elf64DataSource& data,
      std::size_t variableIndex,
      std::size_t& index) const
  {
    RecvPropDisassembler disassembler{};
    RecvPropCall call{};

    try
    {
      call = disassembler.disassemble(data, index);
    }
    catch (const std::exception& ex)
    {
      std::cerr << "warn: disassembly failed in class " << this->m_activeStruct->name() << " for variable #" << std::dec << variableIndex << std::endl;
      throw;
    }

    std::optional<ClientVariable> clientVariable{};

    try
    {
      clientVariable = this->decodeRecvPropCall(data, call);
    }
    catch (...)
    {
      std::cerr << "warn: decoding failed in class " << this->m_activeStruct->name() << " for variable #" << std::dec << variableIndex << std::endl;
      throw;
    }

    return clientVariable;
  }

  std::vector<ClientVariable> DumperClientClass::dumpClientVariables(
      const Elf64DataSource& data,
      std::size_t index) const
  {
    std::vector<ClientVariable> result{};
    bool foundEnd{ false };
    bool skippedShouldNeverSeeThis{ false };

    while (!foundEnd)
    {
      std::optional<ClientVariable> clientVariable{};

      try
      {
        clientVariable = this->dumpRecvPropCall(data, size(result), index);
      }
      catch (...)
      {
        if (!result.empty())
        {
          std::cerr << "warn: previous variable was " << result.back().name() << std::endl;
        }

        throw;
      }

      // Variable registration ends with a call to ___cxa_guard_release.
      foundEnd = (data.at(index + 0x07) == 0xe8);

      if (!skippedShouldNeverSeeThis)
      {
        skippedShouldNeverSeeThis = true;
        continue;
      }

      if (clientVariable)
      {
        result.emplace_back(std::move(clientVariable.value()));
      }
    }

    return result;
  }

  ClientStruct DumperClientClass::dumpClientClassHead(const Elf64DataSource& data, std::size_t index) const
  {
    std::size_t indexLeaNetTableName{ index };

    auto relativeOffsetDataTableName{ data.read<std::uint32_t>(indexLeaNetTableName + 3) };
    auto indexDataTableName{ indexLeaNetTableName + relativeOffsetDataTableName + 0x07 };
    auto dataTableName{ data.readString(indexDataTableName) };

    // std::size_t indexMovNumberOfProperties{ index + 0x07 };
    // auto numberOfProperties{ static_cast<std::size_t>(data.read<std::int32_t>(indexMovNumberOfProperties + 0x01)) };

    auto structName{ this->dataTableNameToClientClassName(dataTableName) };

    return { structName };
  }

  ClientStruct DumperClientClass::dumpClientClassInit(
      const Elf64DataSource& data,
      std::size_t index)
  {
    // Index of the lea instruction that loads pNetTableName in preparation for
    // the call to RecvTable::Construct.
    std::size_t indexLeaNetTableName{ index };

    ClientStruct clientStruct{ this->dumpClientClassHead(data, indexLeaNetTableName) };
    this->m_activeStruct = &clientStruct;

    // Index of the first byte of the preparation of a call to a RecvProp
    // function. Usually a xor.
    std::size_t indexVariableRegistration{};

    if (auto found{ this->findFirstRecvPropCall(data, indexLeaNetTableName) })
    {
      indexVariableRegistration = found.value();
    }
    else
    {
      throw std::runtime_error{ std::string{ "cannot find start of variable registration of ClientClassInit<" } + clientStruct.name() + "::ignored>" };
    }

    std::vector<ClientVariable> clientVariables{ this->dumpClientVariables(data, indexVariableRegistration) };

    this->fillClientStruct(clientStruct, std::move(clientVariables));

    // cppcheck-suppress danglingLifetime
    this->m_activeStruct = nullptr;
    return clientStruct;
  }

  ClientStructHolder DumperClientClass::dump(const Elf64DataSource& data)
  {
    // clang-format off
    constexpr std::array<std::optional<std::uint8_t>, 28> signature{
      0x48, 0x8d, 0x0d, {}, {}, {}, {},
      0xBA, {}, 0x00, 0x00, 0x00,
      0x48, 0x8d, 0x35, {}, {}, {}, {},
      0x48, 0x8d, 0x3d, {}, {}, {}, {},
      0xe8
    };
    // clang-format on

    this->m_recvPropFunctions.clear();

    this->findRecvPropFunctions(data);

    // Finds every ClientClassInit<T::ignored> function.
    auto matches{ data.findAllSignatures(signature, ".text") };

    ClientStructHolder result(size(matches));

    std::transform(matches.begin(), matches.end(), std::inserter(result, result.begin()), [&, this](auto index) {
      return this->dumpClientClassInit(data, index);
    });

    return result;
  }
}