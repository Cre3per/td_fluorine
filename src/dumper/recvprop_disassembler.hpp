#if !defined(HPPtd_fluorine_recvprop_disassembler)
#define HPPtd_fluorine_recvprop_disassembler

#include "data_source/elf64_data_source.hpp"

#include <cstdint>
#include <optional>

namespace td_fluorine
{
  /**
   * Registers that are relevant to calls to RecvProp functions.
   * All addresses are absolute. No relative addresses.
   */
  struct RecvPropCall
  {
    using register_type = std::optional<std::uint64_t>;

    // rdi appears do be a this-pointer
    register_type rsi{}; // Usually pVarName
    register_type rdx{}; // Usually offset
    register_type rcx{}; // Usually sizeofVar
    register_type r8{}; // Usually flags

    std::uint64_t callTarget{};
  };

  class RecvPropDisassembler
  {
  public:
    /**
     * Disassembles a call to a RecvProp function.
     * Throws on error.
     * @param data The data source.
     * @param index The index of the first byte of the preparation to a call to
     *              a RecvProp function. This is usually the index of a xor.
     *              When this call ends and parsing was successful, @index is
     *              the index of the next instruction after the call. Otherwise
     *              it is left unmodified.
     * @return The values of the registers at the point of the call and the
     *         address of the function that gets called.
     */
    [[nodiscard]] RecvPropCall disassemble(const Elf64DataSource& data, std::size_t& index) const;
  };
}

#endif