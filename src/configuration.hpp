#if !defined(HPPtd_fluorine_configuration)
#define HPPtd_fluorine_configuration

#include <string_view>

namespace td_fluorine::configuration
{
  constexpr std::string_view k_projectName{ "td_fluorine" };
}

#endif