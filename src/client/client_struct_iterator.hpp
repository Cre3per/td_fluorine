#if !defined(HPPtd_fluorine_client_struct_iterator)
#define HPPtd_fluorine_client_struct_iterator

#include "client_struct.hpp"

#include <map>
#include <string>

namespace td_fluorine
{
  class client_struct_iterator
  {
  private:
    using map_type = std::map<std::string, ClientStruct>;
    using iterator = map_type::iterator;
    using const_iterator = map_type::const_iterator;

  private:
    iterator m_iterator{};

  public:
    [[nodiscard]] ClientStruct& operator*() noexcept
    {
      return this->m_iterator->second;
    }

    [[nodiscard]] const ClientStruct& operator*() const noexcept
    {
      return this->m_iterator->second;
    }

    client_struct_iterator& operator++() noexcept
    {
      ++this->m_iterator;
      return *this;
    }

  public:
    client_struct_iterator(iterator it)
        : m_iterator{ it }
    {
    }

    client_struct_iterator(const_iterator it)
        : m_iterator{ it }
    {
    }
  };
}

#endif