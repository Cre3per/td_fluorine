#if !defined(HPPtd_flourine_client_struct)
#define HPPtd_flourine_client_struct

#include "client_variable.hpp"

#include <cstddef>
#include <set>
#include <span>
#include <string>
#include <vector>

namespace td_fluorine
{
  class ClientStructHolder;

  class ClientStruct
  {
  public:
    using variables_type = std::set<ClientVariable>;

    using iterator = variables_type::iterator;
    using const_iterator = variables_type::const_iterator;

  private:
    /**
     * Class name.
     */
    std::string m_name{};

    /**
     * Member variables.
     */
    variables_type m_variables{};

    /**
     * Name of the parent struct, if any.
     */
    std::string m_parentName{};

  private:
    /**
     * Checks if the given variable is followed by similar variables that
     * could be converted to an array.
     * If this function returns false, @arrayName and @arrayLength have
     * undefined but safe values.
     * @param clientVariable The variable to perform the check for.
     * @param arrayName Will be set to the array's name. That is, the variable
     *                  name without an array suffix.
     * @param arrayLength Will be set the the array's length.
     * @param arrayVariables Will be set to the number of variables that are
     *                       contained in this array. This is different from
     *                       @arrayLength if the array consists of unrolled
     *                       types.
     * @return True if this and the following variables should be an array.
     */
    bool isVariableFirstMemberOfArray(
        std::set<ClientVariable>::const_iterator clientVariable,
        std::string_view& arrayName,
        std::size_t& arrayLength, std::size_t& arrayVariables) const;

    /**
     * @param clientVariable The variable to perform the check for.
     * @param structName Will be set to the variable's name without the
     *                   identifier of this the member variable.
     * @return True if this variable is a member variable of an unrolled struct.
     */
    [[nodiscard]] bool isMemberOfUnrolledStruct(const ClientVariable& clientVariable);

    /**
     * Checks if the given variable is the first component for an unrolled
     * vector and the two following variable are components of the same vector.
     * @param clientVariable The variable to perform the check for.
     * @param vectorName Will be set to the vector's name.
     * @return True if the given variable is the start of an unrolled vector.
     */
    [[nodiscard]] bool isUnrolledVector(
        std::set<ClientVariable>::const_iterator clientVariable,
        std::string_view& vectorName) const noexcept;

    /**
     * Checks if the given variable is a vector with a missing component which
     * is registered separately.
     * @param clientVariable The variable to perform the check for.
     * @return True if @clientVariable should be a Vector3 and was separated
     *         into a Vector2D and float.
     */
    [[nodiscard]] bool isSadVector(
        std::set<ClientVariable>::const_iterator clientVariable) const noexcept;

    /**
     * Removes client variables and inserts a new one in their place.
     * @param first The first variable to be removed.
     * @param count Number of variables to be removed.
     * @param clientVariable The client variable to be inserted. Offset must
     *                       match that of @first.
     * @return Iterator to the new variable and to the next variable.
     */
    std::pair<const_iterator, const_iterator> replaceVariables(
        std::set<ClientVariable>::const_iterator first,
        std::size_t count,
        ClientVariable&& clientVariable);

    /**
     * Converts a client variable to an array, removing the following
     * @arrayLength variables.
     * @param clientVariable The client variable to be promoted.
     * @param arrayName The name of the array.
     * @param arrayLength Length of the array.
     * @param arrayVariables Number of variables to be consumed by the creation
     *                       of the array.
     * @return Iterator to the new variable and next variable.
     */
    std::pair<const_iterator, const_iterator> makeArray(
        std::set<ClientVariable>::const_iterator clientVariable,
        std::string_view arrayName,
        std::size_t arrayLength, std::size_t arrayVariables);

    /**
     * @param firstMember The client variable to be promoted. This is the
     *                    first member of the struct.
     * @param clientStructs All known structs. Will be filled with structs that
     *                      were extracted while postprocessing this struct.
     * @return Iterator to the new variable and next variable.
     */
    std::pair<const_iterator, const_iterator> makeStruct(
        std::set<ClientVariable>::const_iterator firstMember, ClientStructHolder& clientStructs);

    /**
     * Converts a client variable to a vector, removing the following
     * 2 variables.
     * @param clientVariable The client variable to be promoted.
     * @param vectorName The name of the vector.
     * @return Iterator to the new variable and next variable.
     */
    std::pair<const_iterator, const_iterator> makeVector(
        std::set<ClientVariable>::const_iterator clientVariable,
        std::string_view& vectorName);

    /**
     * Converts a 2d vector into a 3d vector, consuming the following variable.
     * @param clientVariable The vector to be promoted.
     * @return Iterator to the next variable.
     */
    std::pair<const_iterator, const_iterator> growVector(
        std::set<ClientVariable>::const_iterator clientVariable);

    /**
     * Creates a new {@link ClientStruct} based on an unrolled struct.
     * @param firstMember The first member of an unrolled struct.
     * @param clientStructs Will be filled with structs that were extracted during
     *                   postprocessing of this struct.
     * @param extractedVariables Number of variables that were extracted and are
     *                           contained in the new struct.
     * @return The new {@link ClientStruct}. This struct was post-processed.
     */
    ClientStruct extractUnrolledStruct(
        std::set<ClientVariable>::const_iterator firstMember,
        ClientStructHolder& clientStructs,
        std::size_t& extractedVariables);

  public:
    /**
     * @return Name.
     */
    [[nodiscard]] const std::string& name() const noexcept;

    /**
     * @return Variables.
     */
    [[nodiscard]] const std::set<ClientVariable>& variables() const noexcept;

    /**
     * @return Name of the parent struct, or an empty string if there is no
     *         parent.
     */
    [[nodiscard]] const std::string& parentName() const noexcept;

    /**
     * Calculates the total size of this struct.
     * @param clientStructs Used to look up embedded types and parent.
     * @return The total size of the struct. 
     */
    [[nodiscard]] std::size_t size(const ClientStructHolder& clientStructs) const;

    /**
     * Sets the name of the parent struct.
     */
    void parentName(const std::string& parentName);

    /**
     * @param variable The variable to add.
     */
    void addVariable(ClientVariable&& variable);

    /**
     * @param variable The variable to add.
     */
    void addVariable(const ClientVariable& variable);

    /**
     * Merges @clientStruct's variables into this {@link ClientStruct}.
     * Does not verify that the two structs share a name or parent.
     * For each variable (identified by name) that exists in both structs, the
     * variables have to match.
     * @param clientStruct The struct to merge from.
     */
    void mergeFrom(const ClientStruct& clientStruct);

    /**
     * Checks if this struct has a member variable with the given name. If so,
     * returns the member.
     * @param variableName Name to search for.
     * @return The variable named by @variableName or a nullptr.
     */
    [[nodiscard]] const ClientVariable* get_optional(std::string_view variableName) const noexcept;

    /**
     * Checks if this struct has a member variable with the given name.
     * @param variableName Name to search for.
     * @return True if a variable with the given name exists.
     */
    [[nodiscard]] bool hasMember(std::string_view variableName) const noexcept;

    /**
     * @return Names of all structs this struct depends on.
     */
    [[nodiscard]] std::vector<std::string_view> dependencies() const;

    /**
     * Performs various checks on the variable to see if it should be an array,
     * struct, or some other type.
     */
    const_iterator postProcessVariable(const_iterator clientVariable, ClientStructHolder& clientStructs);

    /**
     * Converts groups of similar variables into single variables or arrays.
     * For example, m_vecOrigin.x, m_vecOrigin.y, m_vecOrigin.z will be
     * converted to a single Vector3 m_vecOrigin.
     * @return Unrolled structs that were nested inside of this struct and have
     *         been extracted.
     */
    void postProcess(ClientStructHolder& clientStructs);

  public:
    ClientStruct& operator=(ClientStruct&&) = default;

  public:
    /**
     * @param name Name.
     */
    ClientStruct(std::string_view name);

    ClientStruct(ClientStruct&&) = default;
    ClientStruct(const ClientStruct&) = default;

    ClientStruct() = default;
  };
}

#endif