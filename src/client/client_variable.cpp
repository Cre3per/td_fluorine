#include "client_variable.hpp"

#include "client_struct.hpp"
#include "client_struct_holder.hpp"

#include "exception/bad_variable_merge.hpp"
#include "exception/unknown_struct_error.hpp"

#include "SourceSDK/public/datamap.hpp"

#include <cassert>
#include <map>
#include <set>
#include <span>

namespace td_fluorine
{
  std::string ClientVariable::sanitizeName(std::string&& name)
  {
    static const std::map<std::string_view, std::string_view> renames{
      { "\"player_array\"", "player_array" } // C_Team
    };

    if (auto found{ renames.find(name) }; found != cend(renames))
    {
      name = found->second;
    }

    return std::move(name);
  }

  const std::string& ClientVariable::name() const noexcept
  {
    return this->m_name;
  }

  source_sdk::fieldtype_t ClientVariable::type() const noexcept
  {
    return this->m_type;
  }

  ClientVariable::offset_type ClientVariable::offset() const noexcept
  {
    return this->m_offset;
  }

  bool ClientVariable::isArray() const noexcept
  {
    return (this->m_elements != 0);
  }

  std::size_t ClientVariable::size(const ClientStructHolder& clientStructs) const
  {
    std::size_t arrayMultiplier{ this->isArray() ? this->elements() : 1 };

    if (this->m_type == source_sdk::fieldtype_t::FIELD_EMBEDDED)
    {
      if (const auto* embeddedType{ clientStructs.get_optional(this->m_embeddedType) })
      {
        return (embeddedType->size(clientStructs) * arrayMultiplier);
      }
      else
      {
        throw UnknownStructError{ this->m_embeddedType, *this };
      }
    }
    else
    {
      return (source_sdk::getFieldTypeSize(this->m_type) * arrayMultiplier);
    }
  }

  const std::string& ClientVariable::embeddedType() const noexcept
  {
    return this->m_embeddedType;
  }

  std::size_t ClientVariable::elements() const noexcept
  {
    return this->m_elements;
  }

  void ClientVariable::name(std::string_view name)
  {
    this->m_name = name;
  }

  void ClientVariable::type(source_sdk::fieldtype_t type) noexcept
  {
    this->m_type = type;
  }

  void ClientVariable::offset(offset_type offset) noexcept
  {
    this->m_offset = offset;
  }

  void ClientVariable::embeddedType(const std::string& name)
  {
    this->m_embeddedType = name;
    this->m_type = source_sdk::fieldtype_t::FIELD_EMBEDDED;
  }

  void ClientVariable::elements(std::size_t elements)
  {
    this->m_elements = elements;
  }

  bool ClientVariable::mergeFrom(const ClientVariable& clientVariable)
  {
    bool updated{ false };

    if (this->name() != clientVariable.name())
    {
      throw BadVariableMerge{ clientVariable, *this, "name", clientVariable.name(), this->name() };
    }

    if (this->offset() != clientVariable.offset())
    {
      throw BadVariableMerge{ clientVariable, *this, "offset", clientVariable.offset(), this->offset() };
    }

    if (this->type() != clientVariable.type())
    {
      bool solvedConflict{ false };

      // Maps a small type to all types that can be cast to the small type.
      static const std::map<source_sdk::fieldtype_t, std::set<source_sdk::fieldtype_t>> allowedCasts{
        { source_sdk::fieldtype_t::FIELD_BOOLEAN, { source_sdk::fieldtype_t::FIELD_CHARACTER } },
        { source_sdk::fieldtype_t::FIELD_BOOLEAN, { source_sdk::fieldtype_t::FIELD_INTEGER } },
        { source_sdk::fieldtype_t::FIELD_CHARACTER, { source_sdk::fieldtype_t::FIELD_INTEGER } },
        { source_sdk::fieldtype_t::FIELD_SHORT, { source_sdk::fieldtype_t::FIELD_INTEGER } },
        { source_sdk::fieldtype_t::FIELD_VECTOR, { source_sdk::fieldtype_t::FIELD_POSITION_VECTOR } }
      };

      if (auto found{ allowedCasts.find(this->type()) }; found != cend(allowedCasts))
      {
        if (found->second.contains(clientVariable.type()))
        {
          // Ok, @clientVariable's type can be converted to the current type.
          solvedConflict = true;
        }
      }
      else
      {
        // @clientVariable's type cannot be converted to the current type, but
        // maybe we can convert the current type to @clientVariable's type.
        if (auto found{ allowedCasts.find(clientVariable.type()) }; found != cend(allowedCasts))
        {
          if (found->second.contains(this->type()))
          {
            this->type(clientVariable.type());
            solvedConflict = true;
            updated = true;
          }
        }
      }

      if (!solvedConflict)
      {
        if ((this->type() == source_sdk::fieldtype_t::FIELD_STRING) &&
            (clientVariable.type() == source_sdk::fieldtype_t::FIELD_CHARACTER) &&
            clientVariable.isArray())
        {
          // @clientVariable is a char[]
          this->type(clientVariable.type());
          this->elements(clientVariable.elements());
          solvedConflict = true;
        }
        else if ((this->type() == source_sdk::fieldtype_t::FIELD_CHARACTER) &&
                 this->isArray() &&
                 (clientVariable.type() == source_sdk::fieldtype_t::FIELD_STRING))
        {
          // @this is a char[]
          solvedConflict = true;
        }
      }

      if (!solvedConflict)
      {
        throw BadVariableMerge{ clientVariable, *this, "type", clientVariable.type(), this->type() };
      }
    }

    if (this->elements() != clientVariable.elements())
    {
      if (this->elements() == 0)
      {
        this->elements(clientVariable.elements());
        updated = true;
      }
      else
      {
        if (clientVariable.isArray())
        {
          throw BadVariableMerge{ clientVariable, *this, "array length", clientVariable.elements(), this->elements() };
        }
      }
    }

    if (this->embeddedType().empty())
    {
      if (!clientVariable.embeddedType().empty())
      {
        this->embeddedType(clientVariable.embeddedType());
        updated = true;
      }
    }
    else
    {
      if (!clientVariable.embeddedType().empty() && (this->embeddedType() != clientVariable.embeddedType()))
      {
        throw BadVariableMerge{ clientVariable, *this, "embedded type", clientVariable.embeddedType(), this->embeddedType() };
      }
    }

    return updated;
  }

  ClientVariable::ClientVariable(const std::string& name,
                                 source_sdk::fieldtype_t type,
                                 offset_type offset)
      : m_name{ name },
        m_type{ type },
        m_offset{ offset }
  {
  }

  bool operator==(const ClientVariable& lhs, const ClientVariable& rhs) noexcept
  {
    // TODO: Why don't we compare the other properties?
    return ((lhs.name() == rhs.name()) && (lhs.type() == rhs.type()) && (lhs.offset() == rhs.offset()));
  }

  bool operator!=(const ClientVariable& lhs, const ClientVariable& rhs) noexcept
  {
    return !(lhs == rhs);
  }

  bool operator<(const ClientVariable& lhs, const ClientVariable& rhs) noexcept
  {
    if (lhs.offset() == rhs.offset())
    {
      // Happens with arrays
      return (lhs.name() < rhs.name());
    }
    else
    {
      return (lhs.offset() < rhs.offset());
    }
  }
}