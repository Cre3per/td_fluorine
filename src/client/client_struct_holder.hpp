#if !defined(HPPtd_fluorine_client_client_struct_holder)
#define HPPtd_fluorine_client_client_struct_holder

#include "client_struct.hpp"
#include "map_value_iterator.hpp"

#include <cstddef>
#include <map>
#include <optional>
#include <string>
#include <string_view>

namespace td_fluorine
{
  class ClientStructHolder
  {
  private:
    using container_type = std::map<std::string, ClientStruct>;

  public:
    using iterator = MapValueIterator<container_type>;
    using const_iterator = MapValueIterator<const container_type>;
    using size_type = std::size_t;
    using value_type = ClientStruct;

  private:
    std::map<std::string, ClientStruct> m_structs{};

  public:
    /**
     * @return Number of structs.
     */
    [[nodiscard]] size_type size() const noexcept;

    /**
     * Gets a client struct by name.
     * @param name Name of the client struct to search for.
     * @return The client struct named by @name, if any.
     */
    [[nodiscard]] ClientStruct* get_optional(const std::string& name);
    [[nodiscard]] const ClientStruct* get_optional(const std::string& name) const;

    /**
     * Gets a client struct by name.
     * Throws if no struct with the given name exists.
     * @param name Name of the client struct to search for.
     * @return The client struct named by @name.
     */
    [[nodiscard]] const ClientStruct& get(const std::string& name) const;

    /**
     * @param name Name of the struct to search for.
     * @return True if a struct with the given name exists.
     */
    [[nodiscard]] bool contains(const std::string& name) const;

    /**
     * Adds a new struct .
     * Overrides any existing struct with the same name.
     * @param clientStruct The struct to be added.
     */
    iterator insert(const ClientStruct& clientStruct);
    iterator insert(const_iterator, const ClientStruct& clientStruct);
    iterator insert(ClientStruct&& clientStruct);
    iterator insert(const_iterator, ClientStruct&& clientStruct);

    /**
     * Adds a new struct or merges @clientStruct into an existing struct with
     * the same name.
     * @param clientStruct The struct to be added or updated.
     * @return Iterator to the new or updated element.
     */
    iterator merge(const ClientStruct& clientStruct);

    [[nodiscard]] iterator begin() noexcept;
    [[nodiscard]] iterator end() noexcept;
    [[nodiscard]] const_iterator begin() const noexcept;
    [[nodiscard]] const_iterator end() const noexcept;

    /**
     * Throws if empty.
     * @return The last struct.
     */
    [[nodiscard]] const ClientStruct& back() const;

  public:
    ClientStructHolder() = default;
    /**
     * Reserves spaces for @size structs.
     * @param size Number of structs to reserve space for.
     */
    ClientStructHolder(size_type size);
  };
}

#endif