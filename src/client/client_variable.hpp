#if !defined(HPPtd_flourine_client_variable)
#define HPPtd_flourine_client_variable

#include "SourceSDK/public/datamap.hpp"

#include <cstdint>
#include <string>

namespace td_fluorine
{
  class ClientStruct;
  class ClientStructHolder;

  class ClientVariable
  {
  public:
    using offset_type = std::size_t;

  public:
    /**
     * Sanitizes a variable name, eg. by removing quotation marks.
     * {@link ClientVariable} does not perform any automated name sanitizing.
     * It's up to the individual dumpers to sanitize names.
     * @param name The name to be sanitized.
     * @return The sanitized name.
     */
    [[nodiscard]] static std::string sanitizeName(std::string&& name);

  private:
    /**
     * Name.
     */
    std::string m_name{};

    /**
     * Type.
     */
    source_sdk::fieldtype_t m_type{};

    /**
     * Offset.
     */
    offset_type m_offset{};

    /**
     * If this is an embedded variable, the name of the class.
     */
    std::string m_embeddedType{};

    /**
     * If this is an array, the number of elements.
     */
    std::size_t m_elements{};

  public:
    /**
     * @return Name.
     */
    [[nodiscard]] const std::string& name() const noexcept;

    /**
     * @return Type.
     */
    [[nodiscard]] source_sdk::fieldtype_t type() const noexcept;

    /**
     * @return Offset.
     */
    [[nodiscard]] offset_type offset() const noexcept;

    /**
     * @param clientStructs All client structs.
     * @return Size in bytes of this variable's type. If this variable is an
     *         array, the total size of the array.
     * {@link std::span} requires ElementType to be a complete type.
     */
    [[nodiscard]] std::size_t size(const ClientStructHolder& clientStructs) const;

    /**
     * @return If this is an embedded variable, the class name.
     */
    [[nodiscard]] const std::string& embeddedType() const noexcept;

    /**
     * @return If this is an array, the number of elements. Otherwise 0.
     */
    [[nodiscard]] std::size_t elements() const noexcept;

    /**
     * @return True if this is an array.
     */
    [[nodiscard]] bool isArray() const noexcept;

    /**
     * Renamed the variable.
     * @param name New name.
     */
    void name(std::string_view name);

    /**
     * @param type New type.
     */
    void type(source_sdk::fieldtype_t type) noexcept;

    /**
     * @param offset New offset.
     */
    void offset(offset_type offset) noexcept;

    /**
     * Sets the type of an embedded variable.
     * @param name Class name of the embedded type.
     */
    void embeddedType(const std::string& name);

    /**
     * Sets the number of elements of an array.
     * @param elements Number of elements.
     */
    void elements(std::size_t elements);

    /**
     * Merges @clientVariable into this variable. Only more accurate information
     * is merged.
     * Name and offset have to match.
     * Throws if the merge fails.
     * @param clientVariable The source variable.
     * @return True if the current variable was updated.
     */
    bool mergeFrom(const ClientVariable& clientVariable);

  public:
    /**
     * @param name Name.
     * @param type Type.
     * @param offset Offset.
     */
    ClientVariable(const std::string& name,
                   source_sdk::fieldtype_t type,
                   offset_type offset);

    ClientVariable() = default;
  };

  bool operator==(const ClientVariable& lhs, const ClientVariable& rhs) noexcept;
  bool operator!=(const ClientVariable& lhs, const ClientVariable& rhs) noexcept;
  bool operator<(const ClientVariable& lhs, const ClientVariable& rhs) noexcept;
}

#endif