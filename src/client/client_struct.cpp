#include "client_struct.hpp"

#include "client_struct_holder.hpp"

#include "SourceSDK/public/datamap.hpp"

#include "exception/bad_merge.hpp"
#include "exception/unknown_struct_error.hpp"

#include <algorithm>
#include <charconv>
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

namespace td_fluorine
{
  bool ClientStruct::isVariableFirstMemberOfArray(
      std::set<ClientVariable>::const_iterator clientVariable,
      std::string_view& arrayName,
      std::size_t& arrayLength, std::size_t& arrayVariables) const
  {
    arrayLength = 0;
    arrayVariables = 0;
    std::string_view variableName{ clientVariable->name() };

    if (variableName.empty())
    {
      return false;
    }

    if (variableName.ends_with(']'))
    {
      if (auto openingBracket{ variableName.find_last_of('[') }; openingBracket != std::string_view::npos)
      {
        // We assume that this variable has index 0 without checking it.

        // Include the opening bracket in @arrayName so we can use it to check the
        // following variables.
        arrayName = variableName.substr(0, openingBracket + 1);

        arrayVariables = 1;

        for (auto it{ std::next(clientVariable) }; it != this->m_variables.cend(); std::advance(it, 1))
        {
          if (it->name().starts_with(arrayName))
          {
            // There could be gaps between the variables, eg.
            // m_vecOrigin[0] -> m_vecOrigin[2]
            // or
            // m_vecOrigin[0] at 0x00 and m_vecOrigin[1] at 0x100
            // We assume that this doesn't happen.

            const auto closingBracket{ it->name().find(']', arrayName.size()) };

            if (closingBracket == std::string_view::npos)
            {
              throw std::domain_error{ "Array brace opened but not closed in " + it->name() };
            }

            const auto* indexBegin{ it->name().data() + arrayName.size() };
            const auto* indexEnd{ it->name().data() + closingBracket };

            if (std::from_chars(indexBegin, indexEnd, arrayLength).ptr == indexEnd)
            {
              ++arrayLength;
            }
            else
            {
              throw std::domain_error{ "Cannot parse array index from \"" + std::string(indexBegin, indexEnd) + '\"' };
            }

            ++arrayVariables;
          }
          else
          {
            break;
          }
        }

        // Remove the opening bracket.
        arrayName.remove_suffix(1);
      }
    }

    return (arrayVariables > 0);
  }

  bool ClientStruct::isMemberOfUnrolledStruct(const ClientVariable& clientVariable)
  {
    std::string_view variableName{ clientVariable.name() };

    auto found{ variableName.find_last_of('.') };

    return (found != std::string_view::npos);
  }

  bool ClientStruct::isUnrolledVector(
      std::set<ClientVariable>::const_iterator clientVariable,
      std::string_view& vectorName) const noexcept
  {
    if (clientVariable->type() != source_sdk::fieldtype_t::FIELD_FLOAT)
    {
      return false;
    }

    std::string_view variableName{ clientVariable->name() };

    if (variableName.empty())
    {
      return false;
    }

    if (auto foundDot{ variableName.find('.') }; foundDot != std::string_view::npos)
    {
      if (variableName.substr(foundDot + 1) != "x")
      {
        // Ignore unrolled structs.
        return false;
      }

      // Include the dot in @vectorName so we can use it to check the
      // following variables.
      vectorName = variableName.substr(0, foundDot + 1);

      auto it{ std::next(clientVariable) };

      for (int i{ 0 }; (i < 2) && (it != cend(this->m_variables)); ++i)
      {
        if (it->name().starts_with(vectorName))
        {
          if (i == 1)
          {
            // Remove the trailing dot.
            vectorName.remove_suffix(1);
            return true;
          }
        }
      }
    }

    return false;
  }

  bool ClientStruct::isSadVector(
      std::set<ClientVariable>::const_iterator clientVariable) const noexcept
  {
    if (clientVariable->type() == source_sdk::fieldtype_t::FIELD_VECTOR2D)
    {
      if (auto next{ std::next(clientVariable) }; next != cend(this->m_variables))
      {
        if (next->type() == source_sdk::fieldtype_t::FIELD_FLOAT)
        {
          if (next->name().starts_with(clientVariable->name()))
          {
            return true;
          }
        }
      }
    }

    return false;
  }

  std::pair<ClientStruct::const_iterator, ClientStruct::const_iterator> ClientStruct::replaceVariables(
      std::set<ClientVariable>::const_iterator first,
      std::size_t count,
      ClientVariable&& clientVariable)
  {
    // Get the next iterator before erasing.
    auto nextVariable{ std::next(first, static_cast<decltype(m_variables)::difference_type>(count)) };

    this->m_variables.erase(first, nextVariable);

    // @clientVariable will be inserted at @it, because the offset didn't
    // change. This operation won't affect the following iterators.
    // If @clientArray has the same offset and name as a variable that
    // already exists (eg. C_Local::m_angEyeAngles), @clientArray won't be
    // inserted. That's how it's supposed to be.
    auto newVariable{ this->m_variables.emplace(std::move(clientVariable)) };

    return { newVariable.first, nextVariable };
  }

  std::pair<ClientStruct::const_iterator, ClientStruct::const_iterator> ClientStruct::makeArray(
      std::set<ClientVariable>::const_iterator clientVariable,
      std::string_view arrayName,
      std::size_t arrayLength,
      std::size_t arrayVariables)
  {
    // Can't move from a set.
    auto clientArray{ *clientVariable };

    clientArray.name(arrayName);
    clientArray.elements(arrayLength);

    return this->replaceVariables(clientVariable, arrayVariables, std::move(clientArray));
  }

  std::pair<ClientStruct::const_iterator, ClientStruct::const_iterator> ClientStruct::makeStruct(
      std::set<ClientVariable>::const_iterator firstMember,
      ClientStructHolder& clientStructs)
  {
    std::size_t extractedVariables{};

    auto& newStruct{ *clientStructs.insert(this->extractUnrolledStruct(firstMember, clientStructs, extractedVariables)) };
    newStruct.postProcess(clientStructs);

    std::string_view newVariableName{ firstMember->name() };
    newVariableName.remove_suffix(newVariableName.size() - newVariableName.find_last_of('.'));

    ClientVariable compactedVariable{ std::string{ newVariableName }, source_sdk::fieldtype_t::FIELD_EMBEDDED, firstMember->offset() };
    compactedVariable.embeddedType(newStruct.name());

    return this->replaceVariables(firstMember, extractedVariables, std::move(compactedVariable));
  }

  std::pair<ClientStruct::const_iterator, ClientStruct::const_iterator> ClientStruct::makeVector(
      std::set<ClientVariable>::const_iterator clientVariable,
      std::string_view& vectorName)
  {
    // Can't move.
    auto newVariable{ *clientVariable };

    newVariable.name(vectorName);
    newVariable.type(source_sdk::fieldtype_t::FIELD_POSITION_VECTOR);

    return this->replaceVariables(clientVariable, 3, std::move(newVariable));
  }

  std::pair<ClientStruct::const_iterator, ClientStruct::const_iterator> ClientStruct::growVector(
      std::set<ClientVariable>::const_iterator clientVariable)
  {
    ClientVariable newVariable{ *clientVariable };
    newVariable.type(source_sdk::fieldtype_t::FIELD_VECTOR);

    return this->replaceVariables(clientVariable, 2, std::move(newVariable));
  }

  ClientStruct ClientStruct::extractUnrolledStruct(
      std::set<ClientVariable>::const_iterator firstMember, ClientStructHolder& clientStructs,
      std::size_t& extractedVariables)
  {
    std::string_view baseName{ firstMember->name() };
    baseName.remove_suffix(baseName.size() - baseName.find_last_of('.') - 1);

    std::string structName{ this->name() + '_' + std::string{ baseName.substr(0, baseName.size() - 1) } };

    std::ranges::replace(structName, '[', '_');
    std::ranges::replace(structName, ']', '_');
    std::ranges::replace(structName, '.', '_');

    ClientStruct result{ structName };

    extractedVariables = 0;

    for (auto it{ firstMember }; it != cend(this->m_variables); ++it)
    {
      std::string_view variableName{ it->name() };

      if (variableName.starts_with(baseName))
      {
        // This variable is a member of the same struct.
        ClientVariable clientVariable{ *it };
        clientVariable.offset(clientVariable.offset() - firstMember->offset());
        clientVariable.name(variableName.substr(baseName.length()));

        result.addVariable(std::move(clientVariable));

        ++extractedVariables;
      }
      else
      {
        // The current variable doesn't belong to this struct.
        // Check if the current variable is a second array element. If so, we
        // know the size of the current struct and need padding.
        if (!result.variables().empty() && variableName.substr(0, baseName.size()).ends_with("[1]."))
        {
          const auto expectedSize{ it->offset() - (result.variables().cbegin()->offset() + firstMember->offset()) };

          const auto& lastVariable{ *result.variables().crbegin() };
          const auto currentSize{ lastVariable.offset() + lastVariable.size(clientStructs) };

          if (currentSize < expectedSize)
          {
            const auto requiredPadding{ expectedSize - currentSize };

            std::stringstream padName{};
            padName << "_pad_" << std::hex << currentSize;

            ClientVariable pad{ padName.str(),
                                source_sdk::fieldtype_t::FIELD_CHARACTER, currentSize };

            pad.elements(requiredPadding);

            result.addVariable(std::move(pad));
          }
        }
        break;
      }
    }

    result.postProcess(clientStructs);

    return result;
  }

  const std::string& ClientStruct::name() const noexcept
  {
    return this->m_name;
  }

  const std::set<ClientVariable>& ClientStruct::variables() const noexcept
  {
    return this->m_variables;
  }

  /**
   * {@link m_variables} is sorted by variable offset, so we only need to look
   * at the last variable.
   */
  std::size_t ClientStruct::size(const ClientStructHolder& clientStructs) const
  {
    if (empty(this->m_variables))
    {
      if (empty(this->m_parentName))
      {
        return 1;
      }
      else
      {
        if (const auto* parent{ clientStructs.get_optional(this->m_parentName) })
        {
          return parent->size(clientStructs);
        }
        else
        {
          throw UnknownStructError{ this->m_parentName, *this };
        }
      }
    }
    else
    {
      const auto& last{ *crbegin(this->m_variables) };

      return (last.offset() + last.size(clientStructs));
    }
  }

  const std::string& ClientStruct::parentName() const noexcept
  {
    return this->m_parentName;
  }

  void ClientStruct::addVariable(ClientVariable&& variable)
  {
    this->m_variables.emplace(std::move(variable));
  }

  void ClientStruct::addVariable(const ClientVariable& variable)
  {
    this->m_variables.emplace(variable);
  }

  /**
   * The variable verification is expensive, but good to have.
   * If this function is too slow and merge conflicts are of no concern, the
   * loop's body can be replaced with this->addVariable(variable).
   */
  void ClientStruct::mergeFrom(const ClientStruct& clientStruct)
  {
    std::vector<std::pair<std::set<ClientVariable>::iterator, ClientVariable>> updatedVariables{};

    for (const auto& variable : clientStruct.variables())
    {
      // Check if we already have a variable with the same name.
      auto found{ std::find_if(cbegin(this->m_variables), cend(this->m_variables), [variable](const auto& el) {
        return (el.name() == variable.name());
      }) };

      if (found == cend(this->m_variables))
      {
        this->addVariable(variable);
      }
      else
      {
        auto mergedVariable{ *found };
        bool merged{ false };

        try
        {
          merged = mergedVariable.mergeFrom(variable);
        }
        catch (...)
        {
          std::cerr << "merge conflict while merging struct " << this->name() << ":" << std::endl;
          throw;
        }

        if (merged)
        {
          updatedVariables.emplace_back(found, std::move(mergedVariable));
        }
      }
    }

    for (auto& updated : updatedVariables)
    {
      // std::set doesn't invalidate iterators. This is safe.
      this->m_variables.erase(updated.first);
      this->m_variables.emplace(std::move(updated.second));
    }
  }

  const ClientVariable* ClientStruct::get_optional(std::string_view variableName) const noexcept
  {
    const auto found{ std::ranges::find_if(this->m_variables, [variableName](const auto& clientVariable) {
      return (clientVariable.name() == variableName);
    }) };

    if (found == cend(this->m_variables))
    {
      return nullptr;
    }
    else
    {
      return &*found;
    }
  }

  bool ClientStruct::hasMember(std::string_view variableName) const noexcept
  {
    return (this->get_optional(variableName) != nullptr);
  }

  std::vector<std::string_view> ClientStruct::dependencies() const
  {
    std::vector<std::string_view> dependencies{};

    if (!this->m_parentName.empty())
    {
      dependencies.emplace_back(this->m_parentName);
    }

    for (const auto& clientVariable : this->m_variables)
    {
      if (!clientVariable.embeddedType().empty())
      {
        dependencies.emplace_back(clientVariable.embeddedType());
      }
    }

    return dependencies;
  }

  ClientStruct::const_iterator ClientStruct::postProcessVariable(const_iterator clientVariable, ClientStructHolder& clientStructs)
  {
    std::string_view newName{};
    std::size_t arrayLength{};
    std::size_t arrayVariables{};

    if (this->isVariableFirstMemberOfArray(clientVariable, newName, arrayLength, arrayVariables))
    {
      // If arrays are separated by a rogue variable, avoid duplicate names.
      // We assume that no array is split into more than 2 parts.
      bool isDuplicateName{
        std::ranges::find_if(this->m_variables, [newName, clientVariable](const auto& el) {
          return ((el.name() == newName) && (el.offset() != clientVariable->offset()));
        }) != cend(this->m_variables)
      };

      std::string safeArrayName{
        isDuplicateName ? (std::string{ newName } + '2') : newName
      };

      auto [it, next]{ this->makeArray(clientVariable, safeArrayName, arrayLength, arrayVariables) };

      return this->postProcessVariable(it, clientStructs);
    }
    else if (this->isUnrolledVector(clientVariable, newName))
    {
      auto [it, next]{ this->makeVector(clientVariable, newName) };

      return this->postProcessVariable(it, clientStructs);
    }
    else if (this->isSadVector(clientVariable))
    {
      auto [it, next]{ this->growVector(clientVariable) };
      return this->postProcessVariable(it, clientStructs);
    }
    else if (this->isMemberOfUnrolledStruct(*clientVariable))
    {
      auto [it, next]{ this->makeStruct(clientVariable, clientStructs) };
      return this->postProcessVariable(it, clientStructs);
    }
    else
    {
      return std::next(clientVariable);
    }
  }

  void ClientStruct::postProcess(ClientStructHolder& clientStructs)
  {
    for (auto it{ this->m_variables.cbegin() }; it != this->m_variables.cend();)
    {
      it = this->postProcessVariable(it, clientStructs);
    }
  }

  void ClientStruct::parentName(const std::string& parentName)
  {
    this->m_parentName = parentName;
  }

  ClientStruct::ClientStruct(std::string_view name)
      : m_name{ name }
  {
  }
}