#include "client_struct_holder.hpp"

#include "exception/unknown_struct_error.hpp"

#include <utility>

namespace td_fluorine
{
  ClientStructHolder::size_type ClientStructHolder::size() const noexcept
  {
    return this->m_structs.size();
  }

  ClientStruct* ClientStructHolder::get_optional(const std::string& name)
  {
    if (auto found{ this->m_structs.find(name) }; found != this->m_structs.end())
    {
      return &found->second;
    }
    else
    {
      return nullptr;
    }
  }

  const ClientStruct* ClientStructHolder::get_optional(const std::string& name) const
  {
    // NOLINTNEXTLINE
    return const_cast<ClientStructHolder*>(this)->get_optional(name);
  }

  const ClientStruct& ClientStructHolder::get(const std::string& name) const
  {
    if (const auto* found{ this->get_optional(name) })
    {
      return *found;
    }
    else
    {
      throw UnknownStructError{ name };
    }
  }

  bool ClientStructHolder::contains(const std::string& name) const
  {
    return (this->get_optional(name) != nullptr);
  }

  ClientStructHolder::iterator ClientStructHolder::insert(const ClientStruct& clientStruct)
  {
    return { this->m_structs.emplace(clientStruct.name(), clientStruct).first };
  }

  ClientStructHolder::iterator ClientStructHolder::insert(const_iterator, const ClientStruct& clientStruct)
  {
    return this->insert(clientStruct);
  }

  ClientStructHolder::iterator ClientStructHolder::insert(ClientStruct&& clientStruct)
  {
    return { this->m_structs.emplace(clientStruct.name(), std::move(clientStruct)).first };
  }

  ClientStructHolder::iterator ClientStructHolder::insert(const_iterator, ClientStruct&& clientStruct)
  {
    return this->insert(std::move(clientStruct));
  }

  ClientStructHolder::iterator ClientStructHolder::merge(const ClientStruct& clientStruct)
  {
    if (auto found{ this->m_structs.find(clientStruct.name()) }; found != this->m_structs.end())
    {
      // This is a known struct. Safely merge the new variables into it.
      found->second.mergeFrom(clientStruct);
      return found;
    }
    else
    {
      return this->insert(clientStruct);
    }
  }

  ClientStructHolder::iterator ClientStructHolder::begin() noexcept
  {
    return { this->m_structs.begin() };
  }

  ClientStructHolder::iterator ClientStructHolder::end() noexcept
  {
    return { this->m_structs.end() };
  }

  ClientStructHolder::const_iterator ClientStructHolder::begin() const noexcept
  {
    return { this->m_structs.begin() };
  }

  ClientStructHolder::const_iterator ClientStructHolder::end() const noexcept
  {
    return { this->m_structs.end() };
  }

  const ClientStruct& ClientStructHolder::back() const
  {
    return this->m_structs.crbegin()->second;
  }

  /**
   * Unused, because we're using a {@link std::map}.
   */
  ClientStructHolder::ClientStructHolder(size_type) {}
}