#if !defined(HPPtd_fluorine_map_value_iterator)
#define HPPtd_fluorine_map_value_iterator

#include <map>
#include <type_traits>
#include <utility>

namespace td_fluorine
{
  template <class T>
  class MapValueIterator
  {
  public:
    using map_type = std::remove_cv_t<T>;
    using iterator = std::conditional_t<std::is_const_v<T>, typename map_type::const_iterator, typename map_type::iterator>;
    using difference_type = typename iterator::difference_type;
    using value_type = std::decay_t<decltype(std::declval<iterator>()->second)>;
    using pointer = value_type*;
    using reference = value_type&;
    using iterator_category = typename iterator::iterator_category;

    friend class MapValueIterator<const map_type>;

  private:
    iterator m_iterator{};

  public:
    [[nodiscard]] std::conditional_t<std::is_const_v<T>, const value_type, value_type>& operator*() noexcept
    {
      return this->m_iterator->second;
    }

    [[nodiscard]] auto operator->() noexcept
    {
      return &this->operator*();
    }

    MapValueIterator& operator++() noexcept
    {
      ++this->m_iterator;
      return *this;
    }

    MapValueIterator operator++(int) noexcept
    {
      return { this->m_iterator++ };
    }

    [[nodiscard]] bool operator==(const MapValueIterator& that) const
    {
      return (this->m_iterator == that.m_iterator);
    }

    MapValueIterator& operator=(const MapValueIterator& that)
    {
      this->m_iterator = that.m_iterator;
      return *this;
    }

  public:
    MapValueIterator() = default;

    template <bool IsConst = std::is_const_v<T>, class = std::enable_if_t<IsConst>>
    MapValueIterator(typename map_type::const_iterator it)
        : m_iterator{ it }
    {
    }

    MapValueIterator(typename map_type::iterator it)
        : m_iterator{ it }
    {
    }

    MapValueIterator(const MapValueIterator<map_type>& that)
        : MapValueIterator{ that.m_iterator }
    {
    }
  };
}

#endif