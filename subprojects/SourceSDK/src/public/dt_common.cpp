#include "public/dt_common.hpp"

#include <cassert>

namespace source_sdk
{
  std::ostream& operator<<(std::ostream& os, SendPropType type)
  {
    switch (type)
    {
    case SendPropType::Int:
      os << "int";
      break;
    case SendPropType::Float:
      os << "float";
      break;
    case SendPropType::Vector:
      os << "vector";
      break;
    case SendPropType::VectorXY:
      os << "vectorxy";
      break;
    case SendPropType::String:
      os << "string";
      break;
    case SendPropType::Array:
      os << "array";
      break;
    case SendPropType::InternalArray:
      os << "internalarray";
      break;
    case SendPropType::DataTable:
      os << "datatable";
      break;
    case SendPropType::Int64:
      os << "int64";
      break;
    case SendPropType::EHandle:
      os << "ehandle";
      break;
    case SendPropType::Bool:
      os << "bool";
      break;
    case SendPropType::UtlVector:
      os << "utlvector";
      break;
    default:
      assert(false);
      break;
    }

    return os;
  }
}