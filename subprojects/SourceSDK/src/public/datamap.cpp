#include "public/datamap.hpp"

#include <algorithm>
#include <array>
#include <iterator>
#include <ostream>
#include <stdexcept>
#include <string>
#include <utility>

namespace source_sdk
{
  constexpr std::array<std::string_view, static_cast<std::size_t>(fieldtype_t::FIELD_TYPECOUNT)> typeNames{
    "VOID",
    "FLOAT",
    "STRING",
    "VECTOR",
    "QUATERNION",
    "INTEGER",
    "BOOLEAN",
    "SHORT",
    "CHARACTER",
    "COLOR32",
    "EMBEDDED",
    "CUSTOM",
    "CLASSPTR",
    "EHANDLE",
    "EDICT",
    "POSITION_VECTOR",
    "TIME",
    "TICK",
    "MODELNAME",
    "SOUNDNAME",
    "INPUT",
    "FUNCTION",
    "VMATRIX",
    "VMATRIX_WORLDSPACE",
    "MATRIX3X4_WORLDSPACE",
    "INTERVAL",
    "MODELINDEX",
    "MATERIALINDEX",
    "VECTOR2D",
    "INTEGER64",
    "VECTOR4D",
    "UTLVECTOR"
  };

  std::ostream& operator<<(std::ostream& os, fieldtype_t type)
  {
    auto index{ static_cast<std::size_t>(type) };

    if (index >= size(typeNames))
    {
      throw std::range_error{ "no type name for type with id " + std::to_string(index) };
    }
    else
    {
      os << typeNames[index];
    }

    return os;
  }


  std::optional<fieldtype_t> getFieldTypeByName(std::string_view name) noexcept
  {
    if (auto found{ std::ranges::find(typeNames, name) }; found != cend(typeNames))
    {
      return static_cast<fieldtype_t>(std::distance(cbegin(typeNames), found));
    }

    return {};
  }
}