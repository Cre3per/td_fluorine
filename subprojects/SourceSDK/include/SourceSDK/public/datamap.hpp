#if !defined(HPPsource_sdk_public_datamap)
#define HPPsource_sdk_public_datamap

#include <array>
#include <cstddef>
#include <cstdint>
#include <optional>
#include <ostream>
#include <utility>

namespace source_sdk
{
  class ISaveRestoreOps;
  struct datamap_t;

  enum class fieldtype_t : int
  {
    FIELD_VOID = 0, // No type or value
    FIELD_FLOAT, // Any floating point value
    FIELD_STRING, // A string ID (return from ALLOC_STRING)
    FIELD_VECTOR, // Any vector, QAngle, or AngularImpulse
    FIELD_QUATERNION, // A quaternion
    FIELD_INTEGER, // Any integer or enum
    FIELD_BOOLEAN, // boolean, implemented as an int, I may use this as a hint for compression
    FIELD_SHORT, // 2 byte integer
    FIELD_CHARACTER, // a byte
    FIELD_COLOR32, // 8-bit per channel r,g,b,a (32bit color)
    FIELD_EMBEDDED, // an embedded object with a datadesc, recursively traverse and embedded class/structure based on an additional typedescription
    FIELD_CUSTOM, // special type that contains function pointers to it's read/write/parse functions

    FIELD_CLASSPTR, // CBaseEntity *
    FIELD_EHANDLE, // Entity handle
    FIELD_EDICT, // edict_t *

    FIELD_POSITION_VECTOR, // A world coordinate (these are fixed up across level transitions automagically)
    FIELD_TIME, // a floating point time (these are fixed up automatically too!)
    FIELD_TICK, // an integer tick count( fixed up similarly to time)
    FIELD_MODELNAME, // Engine string that is a model name (needs precache)
    FIELD_SOUNDNAME, // Engine string that is a sound name (needs precache)

    FIELD_INPUT, // a list of inputed data fields (all derived from CMultiInputVar)
    FIELD_FUNCTION, // A class function pointer (Think, Use, etc)

    FIELD_VMATRIX, // a vmatrix (output coords are NOT worldspace)

    // NOTE: Use float arrays for local transformations that don't need to be fixed up.
    FIELD_VMATRIX_WORLDSPACE, // A VMatrix that maps some local space to world space (translation is fixed up on level transitions)
    FIELD_MATRIX3X4_WORLDSPACE, // matrix3x4_t that maps some local space to world space (translation is fixed up on level transitions)

    FIELD_INTERVAL, // a start and range floating point interval ( e.g., 3.2->3.6 == 3.2 and 0.4 )
    FIELD_MODELINDEX, // a model index
    FIELD_MATERIALINDEX, // a material index (using the material precache string table)

    FIELD_VECTOR2D, // 2 floats
    FIELD_INTEGER64, // 64bit integer
    FIELD_VECTOR4D, // 4 floats

    FIELD_UTLVECTOR, // custom. UtlVector with no element type.

    FIELD_TYPECOUNT, // MUST BE LAST
  };

  std::ostream& operator<<(std::ostream& os, fieldtype_t type);

  /**
   * @param type The type.
   * @return Size in bytes of the given type.
   */
  [[nodiscard]] constexpr std::size_t getFieldTypeSize(fieldtype_t type) noexcept
  {
    // The {@link fieldtype_t} pair element is unused. It's there purely as an
    // aid for the developer. Sizes are looked up by index.
    constexpr std::array<std::pair<fieldtype_t, std::size_t>, static_cast<std::size_t>(fieldtype_t::FIELD_TYPECOUNT)> sizes{
      { { fieldtype_t::FIELD_VOID, 8 }, // Incorrect, but we're using void*
        { fieldtype_t::FIELD_FLOAT, 4 },
        { fieldtype_t::FIELD_STRING, 8 },
        { fieldtype_t::FIELD_VECTOR, 3 * 4 },
        { fieldtype_t::FIELD_QUATERNION, 4 * 4 },
        { fieldtype_t::FIELD_INTEGER, 4 },
        { fieldtype_t::FIELD_BOOLEAN, 1 },
        { fieldtype_t::FIELD_SHORT, 2 },
        { fieldtype_t::FIELD_CHARACTER, 1 },
        { fieldtype_t::FIELD_COLOR32, 4 },
        { fieldtype_t::FIELD_EMBEDDED, 0 },
        { fieldtype_t::FIELD_CUSTOM, 8 }, // Incorrect, but we're using void*
        { fieldtype_t::FIELD_CLASSPTR, 8 },
        { fieldtype_t::FIELD_EHANDLE, 4 },
        { fieldtype_t::FIELD_EDICT, 8 },
        { fieldtype_t::FIELD_POSITION_VECTOR, 3 * 4 },
        { fieldtype_t::FIELD_TIME, 4 },
        { fieldtype_t::FIELD_TICK, 4 },
        { fieldtype_t::FIELD_MODELNAME, 8 },
        { fieldtype_t::FIELD_SOUNDNAME, 8 },
        { fieldtype_t::FIELD_INPUT, 4 },
        { fieldtype_t::FIELD_FUNCTION, 8 },
        { fieldtype_t::FIELD_VMATRIX, 4 * 4 * 4 },
        { fieldtype_t::FIELD_VMATRIX_WORLDSPACE, 4 * 4 * 4 },
        { fieldtype_t::FIELD_MATRIX3X4_WORLDSPACE, 4 * 3 * 4 },
        { fieldtype_t::FIELD_INTERVAL, 4 },
        { fieldtype_t::FIELD_MODELINDEX, 4 },
        { fieldtype_t::FIELD_MATERIALINDEX, 4 },
        { fieldtype_t::FIELD_VECTOR2D, 2 * 4 },
        { fieldtype_t::FIELD_INTEGER64, 8 },
        { fieldtype_t::FIELD_VECTOR4D, 4 * 4 },
        { fieldtype_t::FIELD_UTLVECTOR, (8 + 4 + 4) + 4 } }
    };

    return sizes.at(static_cast<std::size_t>(type)).second;
  }

  /**
   * @param name Field name without the "FIELD_" prefix. All caps.
   * @return The found type, or nothing if unmatched.
   */
  [[nodiscard]] std::optional<fieldtype_t> getFieldTypeByName(std::string_view name) noexcept;

  struct typedescription_t
  {
    fieldtype_t fieldType;
    char pad_0x04[4];
    const char* fieldName;
    int fieldOffset;
    std::uint16_t fieldSize;
    std::int16_t flags;
    const char* externalName;
    ISaveRestoreOps* pSaveRestoreOps;
    char pad_0x28[0x10];
    datamap_t* td;
    int fieldSizeInBytes;
    char pad_0x44[0x20];
  }; // size=0x68

  struct datamap_t
  {
    typedescription_t* dataDesc;
    int dataNumFields;
    char pad_0x0C[4];
    const char* dataClassName;
    datamap_t* baseMap;
    bool chains_validated;
    bool packed_offsets_computed;
    int packed_size;
  }; // size=0x28
}

#endif