#if !defined(HPPsource_sdk_dt_common)
#define HPPsource_sdk_dt_common

#include <ostream>

namespace source_sdk
{
  /**
   * This is not the real SendPropType. This is every type for which a RecvProp
   * function exists.
   */
  enum class SendPropType
  {
    Unknown,
    Int,
    Float,
    Vector,
    VectorXY,
    String,
    Array,
    InternalArray,
    DataTable,
    Int64,
    EHandle,
    Bool,
    UtlVector,
  };

  std::ostream& operator<<(std::ostream& os, SendPropType type);
}

#endif