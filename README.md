# td_fluorine

Static netvar dumper for csgo.
Only works with linux x86-64 libraries.

## Usage

To dump client variables to stdout, run
```
./td_fluorine -i ~/.local/share/Steam/steamapps/common/Counter-Strike\ Global\ Offensive/csgo/bin/linux64/client_panorama_client.so
```

### Command line options

See `./td_fluorine --help`

## Intermediate representation

td_fluorine uses a subset of json5 as its intermediate human-readable
representation format. Any json5 parser can parse the output of td_fluorine.
td_fluorine is only guaranteed to be able to parse its own output. Any
modifications to the intermediate representation can cause parsing to fail.
The generated json file can be converted to various languages via
```
# Convert intermediate representation to C++
./td_fluorine -i /path/to/intermediate_representation.json -o /path/to/output.hpp --format cpp
```

### Schema

```json
{
  "type": "array",
  "items": {
    "type": { "$ref": "#/definitions/client_struct"
  },
  "definitions": {
    "client_struct": {
      "type": "object",
      "required": [
        "name",
        "variables"
      ],
      "properties": {
        "name": {
          "type": "string",
          "description": "The name of the client struct"
        },
        "parent": {
          "type": "string",
          "description": "If the client struct has a parent, the name of the parent class"
        },
        "variables": {
          "type": "array",
          "items": {
            "type": "client_variable"
          }
        }
      }
    },
    "client_variable": {
      "type": "object",
      "required": [
        "name",
        "offset",
        "type",
        "elements"
      ],
      "properties": {
        "name": {
          "type": "string",
          "description": "The name of the variable"
        },
        "offset": {
          "type": "number",
          "description": "The hexadecimal offset of the variable"
        },
        "type": {
          "type": "string",
          "description": "The type name of the variable",
        },
        "elements": {
          "type": "number",
          "description": "If this is an array, the number of elements. Otherwise 0"
        }
      }
    }
  }
}
```

For a list of type names, see [datamap.cpp](./lib/SourceSDK/src/public/datamap.cpp).
The `type` of a client variable can also be any other struct name.

# Supported converters 

| Converter/Language                    | Name  |
| ------------------------------------- | ----- |
| C++                                   | cpp   |
| Diff                                  | diff  |
| Extras                                | extra |
| Intermediate representation (Default) | ir    |

## Converter descriptions

### C++

| Option                     | Description                                                                     |
| -------------------------- | ------------------------------------------------------------------------------- |
| --cpp-namespace            | Namespace to place the dump inside.                                             |
| --cpp-sdk-path             | Include path of your source SDK. Used for includes.                             |
| --cpp-sdk-namespace        | Namespace of your source SDK.                                                   |
| --cpp-sdk-no-system        | Do not treat cpp-sdk-path as a system path.                                     |
| --cpp-sdk-header-extension | Extension of source SDK files. Default: `h`                                     |
| --cpp-header-guard         | Name of the macro used as a header guard. Default: `HPP_SOURCE_SDK_NETVAR_DUMP` |
| --cpp-no-assert            | Do not output assertions.                                                       |
| --cpp-no-pragma            | Do not output an alignment pragma. The output will likely be broken.            |

### Diff

Creates a diff from one dump to another (eg. 2 game versions after an update).

| Option         | Description                                                                          |
| -------------- | ------------------------------------------------------------------------------------ |
| --diff-compare | Path to the dump to compare to.                                                      |
| --diff-ignore  | Comma-separated list of changes to be ignored. See the table below for change names. |

Possible changes are

| What                | Description                       |
| ------------------- | --------------------------------- |
| structAdded         | A struct was added.               |
| structRemoved       | A struct was removed.             |
| variableAdded       | A variable was added.             |
| variableRemoved     | A variable was removed.           |
| variableMoved       | A variable's offset changed.      |
| variableTypeChanged | A variable's data type changed.   |
| arraySizeChanged    | An array variable's size changed. |

### Extras

Adds a hardcoded set of variables to the dump which aren't accessible by regular
dumping methods. The offsets of these variables is not hardcoded, they are
placed relative to other variables.

Supported variables
- `C_BaseEntity::m_bDormant`

The output of this converter is the intermediate representation.

## Development notes

### Building

```
mkdir ./build/
cd ./build/
conan install ..
PKG_CONFIG_PATH=. meson
ninja
```

### How it works

This dumper is very easy to break from Valve's side if they wanted to. However,
dumpers don't pose a direct problem to the game's cheater situation and there
are plenty of other dumping methods, which makes it unlikely that Valve
intentionally breaks this dumper.

#### Data descriptions

Prediction- and data maps use `static` `typedescription_t`s for variable storage.
These `typedescription_t`s, as well as the `datamap_t`, which holds the name of
the class, are directly accessible in the binary.  
The `typedescription_t` array and `datamap_t` can be found in the
registration functions

- `PredMapInit`
- `DataMapInit`

When these functions are found, the dumping process of the classes is equivalent
no matter which function registered them.

`PredMapInit<T>` functions have the signature

TODO: Make sure this is correct for all `PredMapInit`s. With(out) basemap. With(out) members.
```
00: push rdb
01: lea  rax, &PredMapInit<T>::predDesc[1]
08: mov  rbp, rsp
0b: pop  rbp
0c: mov  T::m_PredMap, ????
13: mov  T::m_PredMap.dataNumFields, ????
1d: lea  rax, T::m_PredMap
```

`DataMapInit<T>` has many different signatures, see `DumperTypeDescription::collectDataMapInit` 1-5.

#### Client classes, receive properties
Searches for `ClientClassInit<T::ignored>` functions using the signature

```
00: lea rdx, pNetTableName
07: mov edx, nProps
0e: lea rsi, pProps
15: lea rdi, this
1c: call (RecvTable::Construct)
```

`pNetTableName` is the data table name. We need the client class name, but this
is the closest we get. We transform the data table name to a client class name
by replacing the "DT_" prefix with a "C_" prefix.  
`nProps` is the number of properties minus 1, because there's a
should_never_see_this property that gets ignored.  
`pProps` is populated at run-time, which makes it uninteresting to us. The
properties are created in `ClientClassInit<T::ignore>` and can be extracted from
the there.

This signature isn't the first byte in `ClientClassInit<T::ignore>`, but we
don't need to first byte. The signature is located above the calls to the
`RecvProp` functions.

To obtain the variable types, the `RecvProp` functions need to be told apart.
`DumperClientClass::findRecvPropFunctions` searches for every `RecvProp`
function and maps the function address to the variable type the function is
registering. The individual `RecvProp` functions are classified based on their
order in the binary, which is always the same.

Following the call to `RecvTable::Construct`, receive properties are registered
via call to various `RecvProp` functions, one call after the other with no other
operations in between. We disassemble the calls until we encounter a
`call ___cxa_guard_release`, which is always at the end of variable
registration.

#### Fixing up variables

The dumpers as described above find some oddities which need to be fixed to make
the dumps usable.

__Problem__:
Some arrays are unrolled. Rather than being registered as a single array, each
component is registered individually.  
__Solution__:
Collect affected variables based on their name and convert them to an array.

__Problem__:
Some 3d vectors are separated into a 2d vector and a float.  
__Solution__:
Collect affected vectors based on type and name and convert them to a 3d
vector.

__Problem__:
Some 3d vectors are unrolled. They're registered as 3 individual floats with a
.x, .y, and .z suffix.  
__Solution__:
Collect affected vectors based on type and name and convert them to a 3d vector.

__Problem__:
The client variable player_array is registered as "player_array" (including the
quotation marks).
__Solution__:
There's a hardcoded rename map in `ClientVariable::sanitizeName`.

__Problem__:
Some variables of class-type are unrolled. Every member of the type is
registered individually in the struct, rather than registering the entire struct
as a member.  
__Solution__:
Search for unrolled structs based on name. Create a new ClientStruct for every
unrolled struct, and replace the unrolled struct with the new ClientStruct.

__Problem__:
Some variables overlap.
__Solution__:
Converters of languages for which this is an issue have to solve this
themselves. For example, the C++ converter ignores all but the first of a set of
overlapping variables.